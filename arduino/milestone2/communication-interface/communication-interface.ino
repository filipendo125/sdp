#include <SDPArduino.h>
#include <Wire.h>
#include "robot.h"

Robot *robot;

void setup() {
  robot = new Robot();
  Serial.println("awaiting orders...");
}

void loop() {
  robot->robotLoop();
}

void serialEvent() {
  char command = Serial.read();
  for (int i=0; i<100; i++) {
    Serial.print(command);
  }
  
  if (command == 'w') {
    //Serial.println("forward");
    robot->moveRobot(FORWARD);
    
  } else if (command == 's') {
    //Serial.println("backward");
    robot->moveRobot(BACKWARD);
    
  } else if (command == 'd') {
    //Serial.println("clockwise");
    robot->turnRobot(CLOCKWISE);
    
  } else if (command == 'a') {
    //Serial.println("anticlockwise");
    robot->turnRobot(ANTICLOCKWISE);
    
  } else if (command == 'i') {
    //Serial.println("unkicking");
    robot->kickBackward();
    
  } else if (command == 'k') {
    //Serial.println("kick power? (3 digits) ");
    while (Serial.available() < 3);
    int power = Serial.parseInt();
    //Serial.print("kick power ");
    //Serial.println(power);
    robot->kickForward(power);
    
  } else if (command == 'j') {
    //Serial.println("grabbing");
    robot->grab();
    
  } else if (command == 'l') {
    //Serial.println("un-grabbing");
    robot->ungrab();
    
  } else if (command == 't') {
    //Serial.println("forward centimeters? (3 digits)");
    while (Serial.available() < 3);
    int centimeters = Serial.parseInt();
    //Serial.print("forward centimeters ");
    //Serial.println(centimeters);
    robot->moveRobotCentimeters(FORWARD, centimeters);
    
  } else if (command == 'g') {
    //Serial.println("backward centimeters? (3 digits)");
    while (Serial.available() < 3);
    int centimeters = Serial.parseInt();
    //Serial.print("backward centimeters ");
    //Serial.println(centimeters);
    robot->moveRobotCentimeters(BACKWARD, centimeters);
    
  } else if (command == 'h') {
    //Serial.println("clockwise angle? (3 digits)");
    while (Serial.available() < 3);
    int angle = Serial.parseInt();
    //Serial.print("clockwise angle ");
    //Serial.println(angle);
    robot->turnRobotAngle(CLOCKWISE, angle);
    
  } else if (command == 'f') {
    //Serial.println("anticlockwise angle? (3 digits)");
    while (Serial.available() < 3);
    int angle = Serial.parseInt();
    //Serial.print("anticlockwise angle ");
    //Serial.println(angle);
    robot->turnRobotAngle(ANTICLOCKWISE, angle);
    
  } else if (command == ' ') {
    //Serial.println("halting");
    robot->halt();
  }
  
//  // milestone 1-3 code
//  else if (command == 'b') {
//    while (!Serial.available());
//    byte byteToSend = Serial.read();
//    Serial.println(byteToSend);
//    
//    Wire.beginTransmission(0x45);
//    Wire.write(byteToSend);
//    byte result = Wire.endTransmission();
//    Serial.println(result);
//  }
}

