#include "robot.h"

void Robot::moveRobot(int dir) {
  moving = true;
  moveRotations = -1;
  leftWheelDirection = dir;
  rightWheelDirection = dir;
  motorPositions[RIGHTWHEELPOSITION] = 0;
  motorPositions[LEFTWHEELPOSITION]= 0;
}

void Robot::moveRobotRotations(int dir, int rotations) {
  moveRobot(dir); 
  moveRotations = rotations;
}

void Robot::moveRobotCentimeters(int dir, int centimeters) {
  moveRobotRotations(dir, centimetersToRotations(centimeters));
}

void Robot::turnRobot(int dir) {
  moving = true;
  moveRotations = -1;
  leftWheelDirection = dir;
  rightWheelDirection = !dir;
  motorPositions[RIGHTWHEELPOSITION] = 0;
  motorPositions[LEFTWHEELPOSITION]= 0;
}

void Robot::turnRobotRotations(int dir, int rotations) {
  turnRobot(dir);
  moveRotations = rotations;
}

void Robot::turnRobotAngle(int dir, int angle) {
  turnRobotRotations(dir, angleToRotations(angle));
}

