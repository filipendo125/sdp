#include "robot.h"

void Robot::robotLoop() {
  updateMotorPositions();
  doMotors();
  doGrabber();
  doKicker();
}

void Robot::doMotors() {
  if (moving && moveRotations != -1 && (abs(motorPositions[LEFTWHEELPOSITION]) >= moveRotations || abs(motorPositions[RIGHTWHEELPOSITION]) >= moveRotations)) {
    motorStop(LEFTWHEELMOTOR);
    motorStop(RIGHTWHEELMOTOR);
    moving = false;
    for (int i = 0; i < 100; i++) {
      Serial.print('*');
    }
  }
  
  int fullMovePower = 100;
  int adjustedMovePower = 0.90 * fullMovePower;
  if (moving) {
    if (abs(motorPositions[LEFTWHEELPOSITION]) > abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(adjustedMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    } else if (abs(motorPositions[LEFTWHEELPOSITION]) < abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(adjustedMovePower, rightWheelDirection);
    } else {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    }
  }
}

void Robot::doGrabber() {
  if (grabbing) {
    if (grabberTimer >= GRABBINGTIMEOUT) {
      motorStop(GRABBERMOTOR);
      grabbing = false;
      grabbed = true;
    }
    grabberTimer++;
  } else if (ungrabbing) {
    if (grabberTimer >= UNGRABBINGTIMEOUT) {
      motorStop(GRABBERMOTOR);
      ungrabbing = false;
      grabbed = false;
    }
    grabberTimer++;
  } else if (grabbed && moving && leftWheelDirection != rightWheelDirection) {
      if (GRABBERORIENTATION == 0) {
        motorForward(GRABBERMOTOR, 10);
      } else if (GRABBERORIENTATION == 1) {
        motorBackward(GRABBERMOTOR, 10);
      }
  } else {
    motorStop(GRABBERMOTOR);
  }
}

void Robot::doKicker() {
  if (kicking) {
    if (kickerTimer >= KICKINGTIMEOUT) {
      motorStop(KICKERMOTOR);
      kicking = false;
    }
    kickerTimer++;
  } else if (unkicking) {
    if (kickerTimer >= UNKICKINGTIMEOUT) {
      motorStop(KICKERMOTOR);
      unkicking = false;
    }
    kickerTimer++;
  }
}
