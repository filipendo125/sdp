#include "robot.h"

void Robot::updateMotorPositions() {
  // Request motor position deltas from rotary slave board
  Wire.requestFrom(ROTARY_SLAVE_ADDRESS, ROTARY_COUNT);
  
  // Update the recorded motor positions
  for (int i = 0; i < ROTARY_COUNT; i++) {
    motorPositions[i] += (int8_t) Wire.read();  // Must cast to signed 8-bit type
  }
}

void Robot::printMotorPositions() {
  Serial.print("LW: ");
  Serial.print(motorPositions[LEFTWHEELPOSITION]);
  
  Serial.print(", RW: ");
  Serial.print(motorPositions[RIGHTWHEELPOSITION]);
}

int Robot::centimetersToRotations(int centimeters) {
  return round(centimeters * 90 / 100);
  // 3.D04 = 1.31
}

int Robot::angleToRotations(int angle) {
  return round((angle * 42) / 360);
  // 3.D04 = 66 rotations per 360 degrees
}
