#ifndef HEADER_ROBOT
  #define HEADER_ROBOT
  
  #include <Arduino.h>
  #include <SDPArduino.h>
  #include <Wire.h>
  
  #define ROTARY_COUNT 6
  #define ROTARY_SLAVE_ADDRESS 5
  
  #define FORWARD 0
  #define BACKWARD 1
  #define CLOCKWISE 0
  #define ANTICLOCKWISE 1
  
  #define LEFTWHEELMOTOR 4
  #define LEFTWHEELORIENTATION 0
  #define LEFTWHEELPOSITION 2
  
  #define RIGHTWHEELMOTOR 2
  #define RIGHTWHEELORIENTATION 1
  #define RIGHTWHEELPOSITION 1
  
  #define KICKERMOTOR 3
  #define KICKERORIENTATION 0
  
  #define GRABBERMOTOR 0
  #define GRABBERORIENTATION 0
  
  #define GRABBINGTIMEOUT 200
  #define UNGRABBINGTIMEOUT 400
  
  #define KICKINGTIMEOUT 200
  #define UNKICKINGTIMEOUT 200
  
  class Robot {
    public:
      int motorPositions[ROTARY_COUNT];
      boolean moving;
      boolean leftWheelDirection;
      boolean rightWheelDirection;
      int moveRotations;
      
      boolean grabbing;
      boolean ungrabbing;
      boolean grabbed;
      int grabberTimer;
      
      boolean kicking;
      boolean unkicking;
      int kickerTimer;
      
      Robot();
      void setupKicker();
      void setupGrabber();
      void setupMotors();
      
      void robotLoop();
      void doMotors();
      void doGrabber();
      void doKicker();
      
      void moveLeftWheel(int power, int dir);
      void moveRightWheel(int power, int dir);
      void moveLeftWheelForward(int power);
      void moveLeftWheelBackward(int power);
      void moveRightWheelForward(int power);
      void moveRightWheelBackward(int power);
      void halt();
      
      void moveRobot(int dir);
      void moveRobotRotations(int dir, int rotations);
      void moveRobotCentimeters(int dir, int centimeters);
      void turnRobot(int dir);
      void turnRobotRotations(int dir, int rotations);
      void turnRobotAngle(int dir, int angle);
      
      void kickForward(int power);
      void kickBackward();
      void grab();
      void ungrab();

      void updateMotorPositions();
      void printMotorPositions();
      int centimetersToRotations(int centimeters);
      int angleToRotations(int angle);
  };
#endif

