#include "robot.h"

void Robot::kickForward(int power) {
  ungrab();
  if (KICKERORIENTATION == 0) {
    motorForward(KICKERMOTOR, 100);
  } else if (KICKERORIENTATION == 1) {
    motorBackward(KICKERMOTOR, 100);
  }
  kicking = true;
  kickerTimer = 0;
}

void Robot::kickBackward() {
  if (KICKERORIENTATION == 0) {
    motorBackward(KICKERMOTOR, 100);
  } else if (KICKERORIENTATION == 1) {
    motorForward(KICKERMOTOR, 100);
  }
  unkicking = true;
  kickerTimer = 0;
}

void Robot::grab() {
    if (GRABBERORIENTATION == 0) {
      motorForward(GRABBERMOTOR, 100);
    } else if (GRABBERORIENTATION == 1) {
      motorBackward(GRABBERMOTOR, 100);
    }
    grabbing = true;
    grabberTimer = 0;
}

void Robot::ungrab() {
  if (GRABBERORIENTATION == 0) {
    motorBackward(GRABBERMOTOR, 80);
  } else if (GRABBERORIENTATION == 1) {
    motorForward(GRABBERMOTOR, 80);
  }
  ungrabbing = true;
  grabberTimer = 0;
}

