#include "robot.h"

Robot::Robot() {
  memset(motorPositions, 0, sizeof(motorPositions));
  SDPsetup();
  halt();
  setupKicker();
  setupGrabber();
}

void Robot::setupMotors() {
  moving = false;
}

void Robot::setupKicker() {
  kicking = false;
  unkicking = false;
  kickerTimer = 0;
  kickBackward();
}

void Robot::setupGrabber() {
  grabbing = false;
  ungrabbing = false;
  grabberTimer = 0;
  ungrab();
}

