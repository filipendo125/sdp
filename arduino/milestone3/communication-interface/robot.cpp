#include "robot.h"

Robot::Robot(Sender *theSender) {
  sender = theSender;
  memset(motorPositions, 0, sizeof(motorPositions));
  SDPsetup();
  setupMotors();
  setupKicker();
  setupGrabber();
}

void Robot::setupMotors() {
  halt();
}

void Robot::setupKicker() {
  motorStop(KICKERMOTOR);
  kicking = false;
  unkicking = false;
  kickBackward(); // unkick by default
}

void Robot::setupGrabber() {
  motorStop(GRABBERMOTOR);
  grabbing = false;
  ungrabbing = false;
  grab(); // grab by default
}


void Robot::robotLoop() {
  updateMotorPositions();
  doMotors();
  doGrabber();
  doKicker();
}

void Robot::doMotors() {
  // if we are moving a set number of rotations, and have finished, stop and send the 'done' message
  if (moving && moveRotations != -1 && (abs(motorPositions[LEFTWHEELPOSITION]) >= moveRotations && abs(motorPositions[RIGHTWHEELPOSITION]) >= moveRotations)) {
    motorStop(LEFTWHEELMOTOR);
    motorStop(RIGHTWHEELMOTOR);
    moving = false;
    sender->sendByte('*');
  }
  
  if (moving) {
    int fullMovePower = 100;
    int adjustedMovePower = 0.90 * fullMovePower;
    // if the left wheel has moved more than the right, reduce the left wheel's power
    if (abs(motorPositions[LEFTWHEELPOSITION]) > abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(adjustedMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    // if the right wheel has moved more than the left, reduce the right wheel's power
    } else if (abs(motorPositions[LEFTWHEELPOSITION]) < abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(adjustedMovePower, rightWheelDirection);
    // otherwise, keep going full power
    } else {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    }
  }
}

void Robot::doGrabber() {
  unsigned long timestamp = millis();
  if (grabbing) {
    // keep grabbing until the timer runs out
    if (timestamp - grabberTimestamp >= GRABBINGTIMEOUT) {
      motorStop(GRABBERMOTOR);
      grabbing = false;
      reboundGrab();
    }
  } else if (reboundgrabbing) {
    // keep rebound grabbing until the timer runs out
    if (timestamp - grabberTimestamp >= REBOUNDGRABBINGTIMEOUT) {
      motorStop(GRABBERMOTOR);
      reboundgrabbing = false;
      grabbed = true;
    }
  } else if (ungrabbing) {
    // keep ungrabbing until the timer runs out
    if (timestamp - grabberTimestamp >= UNGRABBINGTIMEOUT) {
      motorStop(GRABBERMOTOR);
      ungrabbing = false;
      grabbed = false;
    }
  } else if (grabbed && moving && leftWheelDirection != rightWheelDirection) {
      // grab a little whilst turning
      moveGrabberForward(10);
  } else {
    motorStop(GRABBERMOTOR);
  }
}

void Robot::doKicker() {
  unsigned long timestamp = millis();
  if (kicking) {
    // keep kicking until the timer runs out
    if (timestamp - kickerTimestamp >= KICKINGTIMEOUT) {
      motorStop(KICKERMOTOR);
      kicking = false;
    }
  } else if (unkicking) {
    // keep unkicking until the timer runs out
    if (timestamp - kickerTimestamp >= UNKICKINGTIMEOUT) {
      motorStop(KICKERMOTOR);
      unkicking = false;
      reboundKickBackward();
    }
  } else if (reboundUnkicking) {
    // keep unkicking until the timer runs out
    if (timestamp - kickerTimestamp >= REBOUNDUNKICKINGTIMEOUT) {
      motorStop(KICKERMOTOR);
      reboundUnkicking = false;
    }
  }
}
