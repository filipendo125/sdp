#include "robot.h"

void Robot::kickForward(int power) {
  ungrab();
  moveKickerForward(power);
  kicking = true;
  unkicking = false;
  reboundUnkicking = false;
  kickerTimestamp = millis();
}

void Robot::kickBackward() {
  moveKickerBackward(80);
  unkicking = true;
  kickerTimestamp = millis();
}

void Robot::reboundKickBackward() {
  moveKickerBackward(30);
  reboundUnkicking = true;
  kickerTimestamp = millis();
}

void Robot::grab() {
  moveGrabberForward(100);
  grabbing = true;
  grabberTimestamp = millis();
}

void Robot::reboundGrab() {
  moveGrabberForward(10);
  reboundgrabbing = true;
  grabberTimestamp = millis();
}

void Robot::ungrab() {
  moveGrabberBackward(80);
  ungrabbing = true;
  grabberTimestamp = millis();
}

