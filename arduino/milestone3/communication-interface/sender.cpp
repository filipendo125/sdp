#include "sender.h"

Sender::Sender() {
  sequenceByte = SENDER_SEQUENCE_START;
  sending = false;
  latestSendTime = millis();
  
  // flush input buffer
  while (Serial.available() > 0) {
    Serial.read();
  }
  
  // flush output buffer
  Serial.flush();
}

void Sender::sendByte(byte theByte) {
  byteToSend = theByte;
  checksumByte = ~theByte; // calculate the checksum byte such that byteToSend + checksumByte = 255
  sending = true;
}

void Sender::senderLoop() {
  // send the command to be sent, every RETRANSMISSION_TIMEOUT seconds, until acknowledged
  if (sending && millis() - latestSendTime >= RETRANSMISSION_TIMEOUT) {
    Serial.write(sequenceByte);
    Serial.write(byteToSend);
    Serial.write(checksumByte);
    latestSendTime = millis();
  }  
}

void Sender::byteReceived(byte receivedByte) {
  if (sending && receivedByte == sequenceByte) { // the sequence byte received matches the one just sent
    sending = false;
    // alternate the sequence byte
    if (sequenceByte == SENDER_SEQUENCE_START || sequenceByte == SENDER_SEQUENCE_1) {
      sequenceByte = SENDER_SEQUENCE_0;
    } else if (sequenceByte == SENDER_SEQUENCE_0) {
      sequenceByte = SENDER_SEQUENCE_1;
    }
  }
}
