#ifndef HEADER_ROBOT
  #define HEADER_ROBOT
  
  #include <Arduino.h>
  #include <SDPArduino.h>
  #include <Wire.h>
  #include "sender.h"
  
  #define ROTARY_COUNT 6
  #define ROTARY_SLAVE_ADDRESS 5
  
  #define FORWARD 0
  #define BACKWARD 1
  #define CLOCKWISE 0
  #define ANTICLOCKWISE 1
  
  #define LEFTWHEELMOTOR 4
  #define LEFTWHEELORIENTATION 0
  #define LEFTWHEELPOSITION 2
  
  #define RIGHTWHEELMOTOR 2
  #define RIGHTWHEELORIENTATION 1
  #define RIGHTWHEELPOSITION 1
  
  #define KICKERMOTOR 3
  #define KICKERORIENTATION 1
  #define KICKERPOSITION 0
  
  #define GRABBERMOTOR 0
  #define GRABBERORIENTATION 0
  
  #define GRABBINGTIMEOUT 150
  #define REBOUNDGRABBINGTIMEOUT 300
  #define UNGRABBINGTIMEOUT 120
  
  #define KICKINGTIMEOUT 300
  #define UNKICKINGTIMEOUT 250
  #define REBOUNDUNKICKINGTIMEOUT 400
  
  class Robot {
    public:
      Sender *sender;
      int motorPositions[ROTARY_COUNT];
      boolean moving;
      boolean leftWheelDirection;
      boolean rightWheelDirection;
      int moveRotations;
      
      boolean kicking;
      boolean unkicking;
      boolean reboundUnkicking;
      unsigned long kickerTimestamp;
      
      boolean grabbing;
      boolean ungrabbing;
      boolean reboundgrabbing;
      boolean grabbed;
      unsigned long grabberTimestamp;
      
      Robot(Sender *theSender);
      void setupKicker();
      void setupGrabber();
      void setupMotors();
      
      void robotLoop();
      void doMotors();
      void doGrabber();
      void doKicker();
      
      void moveLeftWheel(int power, int dir);
      void moveRightWheel(int power, int dir);
      void moveLeftWheelForward(int power);
      void moveLeftWheelBackward(int power);
      void moveRightWheelForward(int power);
      void moveRightWheelBackward(int power);
      void moveKickerForward(int power);
      void moveKickerBackward(int power);
      void moveGrabberForward(int power);
      void moveGrabberBackward(int power);
      void halt();
      
      void moveRobot(int dir);
      void moveRobotRotations(int dir, int rotations);
      void turnRobot(int dir);
      void turnRobotRotations(int dir, int rotations);
      
      void kickForward(int power);
      void kickBackward();
      void reboundKickBackward();
      void grab();
      void reboundGrab();
      void ungrab();

      void updateMotorPositions();
      void printMotorPositions();
  };
#endif

