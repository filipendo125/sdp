#include "receiver.h"

Receiver::Receiver(Robot *theRobot) {
  robot = theRobot;
  communicationState = AWAITING_SEQUENCE_BYTE;
  sequenceStateSet = false;
  sequenceState = RECEIVER_SEQUENCE_START;
  currentDigit = 4;
  totalReceived = 0;
}

void Receiver::byteReceived(byte receivedByte) {
  // if the byte is the start of a message, get ready for the rest of the message
  if (receivedByte == RECEIVER_SEQUENCE_0 || receivedByte == RECEIVER_SEQUENCE_1) {
    sequenceByte = receivedByte;
    communicationState = AWAITING_COMMAND_BYTE;
  // particularly, if the byte is the start of a 'handshake' message, the sequence state will need to be reset
  } else if (receivedByte == RECEIVER_SEQUENCE_START) {
    sequenceByte = receivedByte;
    communicationState = AWAITING_COMMAND_BYTE;
    sequenceStateSet = false;
  // if we have just received the start of a message, then the next byte is the command byte, and the following byte should be the checksum
  } else if (communicationState == AWAITING_COMMAND_BYTE) {
    commandByte = receivedByte;
    communicationState = AWAITING_CHECKSUM_BYTE;
  // if we get the checksum byte, process the complete message
  } else if (communicationState == AWAITING_CHECKSUM_BYTE) {
    checksumByte = receivedByte;
    communicationState = AWAITING_SEQUENCE_BYTE;
    if (commandByte + checksumByte == 127) { // make sure the checksum checks out
      Serial.write(sequenceByte); // send the acknowledgement
      if (!sequenceStateSet) { // it was a handshake message, so reset the sequence state and follow the command
        sequenceState = RECEIVER_SEQUENCE_0;
        sequenceStateSet = true;
        commandReceived(commandByte); // follow the command
      } else if (sequenceByte == sequenceState) { // only follow the command if this message was not a duplicate
        // alternate the sequence state
        if (sequenceState == RECEIVER_SEQUENCE_0) {
          sequenceState = RECEIVER_SEQUENCE_1;
        } else if (sequenceState == RECEIVER_SEQUENCE_1) {
          sequenceState = RECEIVER_SEQUENCE_0;
        }
        commandReceived(commandByte); // follow the command
      }
    }
  }
}

void Receiver::commandReceived(byte command) {
  if (command == '0' || command == '1' || command == '2' || command == '3' || command == '4' ||
      command == '5' || command == '6' || command == '7' || command == '8' || command == '9') {
    if (1 <= currentDigit && currentDigit <= 3 ) { // if we are expecting a digit
      int singleDigitReceived = command - 48; // ASCII -> digit
      totalReceived += singleDigitReceived * pow(10, currentDigit - 1); // keep a running total
      if (currentDigit == 1) { // we are done with the digits, execute the full command
        if (commandType == 't') {
          robot->moveRobotRotations(FORWARD, totalReceived);
          
        } else if (commandType == 'g') {
          robot->moveRobotRotations(BACKWARD, totalReceived);
          
        } else if (commandType == 'h') {
          robot->turnRobotRotations(CLOCKWISE, totalReceived);
          
        } else if (commandType == 'f') {
          robot->turnRobotRotations(ANTICLOCKWISE, totalReceived);
        
        } else if (commandType == 'b' && 0 <= totalReceived && totalReceived <= 100) {
          robot->kickForward(totalReceived);
        }
        currentDigit = 4; // we are not expecting a digit anymore
      } else {
        currentDigit--; // expect the next digit
      }
    }
  } else {
    currentDigit = 4;
    totalReceived = 0;
    if (command == 'w') {
      robot->moveRobot(FORWARD);
      
    } else if (command == 's') {
      robot->moveRobot(BACKWARD);
      
    } else if (command == 'd') {
      robot->turnRobot(CLOCKWISE);
      
    } else if (command == 'a') {
      robot->turnRobot(ANTICLOCKWISE);
      
    } else if (command == 'i') {
      robot->kickBackward();
      
    } else if (command == 'k') {
      robot->kickForward(100);
      
    } else if (command == 'j') {
      robot->grab();
      
    } else if (command == 'l') {
      robot->ungrab();
    
    } else if (command == ' ') {
      robot->halt();
    }
    
    else if (command == 't' || command == 'g' || command == 'h' || command == 'f' || command == 'b') {
      commandType = command; // remember what command was sent
      currentDigit--; // we are now expecting a digit
    }
  }
}

