#ifndef HEADER_SENDER
  #define HEADER_SENDER
  
  #include <Arduino.h>
  
  #define SENDER_SEQUENCE_START 240 // the byte that resets the sequence state
  #define SENDER_SEQUENCE_0 85 // a sequence state
  #define SENDER_SEQUENCE_1 170 // the other, alternated sequence state
  
  #define RETRANSMISSION_TIMEOUT 1000 // in milliseconds
  
  class Sender {
    public:
      byte sequenceByte; // the sequence number of packet to send
      byte byteToSend; // the byte to send (content of the packet)
      byte checksumByte; // the corresponding checksum byte (byteToSend + checksumByte = 255)
      boolean sending; // are we currently sending a bye
      unsigned long latestSendTime; // when was the last time we sent it?
    
      Sender();
      void senderLoop();
      void sendByte(byte theByte);
      void byteReceived(byte receivedByte);
  };
  
#endif
