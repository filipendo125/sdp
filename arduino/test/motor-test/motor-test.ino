#include "SDPArduino.h"
#include <Wire.h>
                                                    
void setup(){
  SDPsetup();
}

void loop(){
  for (int i = 0; i < 6; i++) {
    if (i > 5) {
      i = 0;
    }
    Serial.print("Motor: ");
    Serial.println(i);
    motorBackward(i, 50);
    delay(1000);
    motorStop(i);
  }
}
