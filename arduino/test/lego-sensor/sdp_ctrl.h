/**
 * @author Alistair John Strachan <alistair@devzero.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __SDP_CTRL_H
#define __SDP_CTRL_H

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

// lm_sensors header
//#include <devbot/core/i2c-dev.h>
#include "i2c-dev.h"
//#include <core/i2c-dev.h>

/** Error macro */
#define error(x...)	fprintf(stderr, ##x)

/** which motor to utilise */
typedef enum {
	m1	= 0x1,
	m2	= 0x3,
	m3	= 0x5,
	m4	= 0x7,
} Motor;

/** direction to put motor in */
typedef enum {
	flt	= 0x00,	// ??
	fwd	= 0x01,	// forward
	rev	= 0x02,	// reverse
	stop	= 0x03,	// stop
} MotorDir;

/** LEDs to light */
typedef enum {
	off	= 0x00,
	led1	= 0x01,
	led2	= 0x02,
	led3	= 0x04,
	led4	= 0x08,
	on	= 0xff,
} Led;

/** Switches to check */
typedef enum {
	sw1	= 0x10,
	sw2	= 0x20,
	sw3	= 0x40,
	sw4	= 0x80,
} Switch;

/** Channel to check ?? */
typedef enum {
	ch1	= 0x01,
	ch2	= 0x02,
	ch3	= 0x03,
	ch4	= 0x04,
} Channel;

/** Sensor identifications */
typedef enum {
	sen0	= 0x00,
	sen1	= 0x01,
	sen2	= 0x02,
	sen3	= 0x03,
	sen4	= 0x04,
	sen5	= 0x05,
	sen6	= 0x06,
} Sensor;

typedef enum {
	adc0	= 0x43,
	adc1	= 0x83,
} Adc;

typedef enum {
	fast	= 0x80,
	slow	= 0x90,
} Mode;


/** function prototypes */
int init_i2c(void);
void exit_i2c(int device);
void probe(void);
int read_motors(int *handle, Motor motor);
int set_motors(int *handle, Motor motor, MotorDir dir, int speed);
int write_8574(int *handle, Led led);
int read_8574(int *handle, Switch sw);
int read_8591(int *handle, Channel ch);
int read_aboard_sensor(int *handle, Sensor ch);
int read_aboard_rotation(int *handle, Sensor ch);
int read_pboard_sensor(int *handle, Sensor ch);
int set_pboard_IO(int *handle, int pins);
int read_light_sensor(int *handle, Adc adc); // Single TSL2550 device
int read_lboard_sensors(int *handle, Mode mode); // Dual TSL2550 PIC interface
int write_lboard_byte(int *handle, char i2c_byte);

#endif // __SDP_CTRL_H
