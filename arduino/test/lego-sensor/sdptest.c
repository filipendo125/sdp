/**
 * Hacked to work with SDP library rewrite.


Note: Memory Test - Board mem is not configured at 0-64MB.
 */


/*
	if((mem_ptr = malloc(mem_size * sizeof (char)))==NULL){
		printf("malloc error\n");
		}


	mem_ptr = malloc(mem_size * sizeof (char));
	if (mem_ptr==NULL) {
		printf("malloc error\n");
		}

*/
#include <string.h> //needed for strcmp
#include <stdlib.h>
#include <curses.h>
#include "sdp_ctrl.h"
#include <errno.h>



void plot_block(long mem_base, long mem_address, char val);


void curses_test(void)
{
	int x_pos, y_pos, val, blocks_prev, blocks, diff_count;
	int mem_count, mem_base_address;


	if((initscr())==NULL){
	    printf("curses failed!\n");
	    exit(EXIT_FAILURE);
	    }

	// test the plot block routine - fill area
	for(mem_count=0; mem_count < 0x3FFFFFF; mem_count+=0x10000){
		plot_block((long) mem_base_address, mem_count+mem_base_address, '0');
		}
	sleep(4);
	printf("FINISHED\n");
//	cbreak();
	refresh();
	move(0,0);
	addstr("This is the first line");
	mvprintw(20,0,"x=%3d, y=%3d", x_pos, y_pos);

	addch('.'|A_REVERSE);
	move(0,1);
	addstr("This is the second line");
	refresh();
	nocbreak();
	endwin();

	sleep(2);
	printf("FINISHED2\n");
}


// Assumptions: total mem is 64MB, but addresses are not simply
// mapped as 0-64MB. Need to find out how to get the base address...
// plot val at offset mem address map
void plot_block(long mem_base, long mem_address, char val)
{
	static	int blocks_prev;
	static	char val_prev;
	static  long same_count=0, diff_count=0;
	int x_pos, y_pos, blocks;

	mem_address=(mem_address-mem_base) & 0x03ffffff; // limit to 64MB
	blocks = mem_address >> 16; //get rid of lower 64KB
	
	if(!((blocks_prev == blocks)&&(val_prev == val))){
		x_pos=blocks & 0x3F;
		y_pos=(blocks >> 6) & 0xF;

		mvaddch(y_pos+2, x_pos+8, val|A_NORMAL);
		blocks_prev=blocks;	// save 
		val_prev=val;
		diff_count++;
		mvprintw(20,0,"x=%3d, y=%3d", x_pos, y_pos);
		mvprintw(21,0,"base=%8X, address=%8X", mem_base, mem_address);
//		sleep(1);
	}
	else	{
		same_count++;
		//mvprintw(22,0,"s=%5d", ++same_count);
		//refresh();
		}
	refresh();
}



typedef unsigned int datum; /* Set the data bus width to X bits */


/**********************************************************************
 *
 * Function:    memTestDataBus()
 *
 * Description: Test the data bus wiring in a memory region by
 *              performing a walking 1's test at a fixed address
 *              within that region.  The address (and hence the
 *              memory region) is selected by the caller.
 *
 * Notes:       
 *
 * Returns:     0 if the test succeeds.  
 *              A non-zero result is the first pattern that failed.
 *
 **********************************************************************/
datum memTestDataBus(volatile datum *address, long mem_size)
{
    datum pattern, read_data;
	int mem_count;
	int mem_base_address = (int) address;
	int error_count = 0;	//error counter
	printf("\nData Bus Walking '1's Test started at 0x%X\n", address);

	// test the plot block routine - fill area
/*
	for(mem_count=0; mem_count < 0x3FFFFFF; mem_count+=0x10000){
		plot_block((long) mem_base_address, mem_count+mem_base_address, '0');
		}
	sleep(2);
*/

	while(mem_size>0)
	{
//	plot_block((long) mem_base_address, (long) address, 'x');
	    for (pattern = 1; pattern != 0; pattern <<= 1)
	    {
	//	printf("DEBUG - 0x%X 0x%X\n",address, pattern);
	        *address = pattern;
	        if ((read_data = *address) != pattern) 
	        {
		    printf("Mem error at 0x%X - wrote 0x%X, Read 0x%X\n", 
			address, pattern, read_data);
	            error_count++;
	        }
	    }
	mem_size--;
	address++;
	}
	printf("Walking '1's Finished at 0x%X\n\n",--address);
    return (error_count);

}   /* memTestDataBus() */
              





/*
Simple memory tester

 - Allocates the biggest contiguous chunk of memory, by repeatedly calling
   malloc (and free) until call returns failure. Encountered a problem with
   the process being unexpectedly terminated, probably due to not having
   left enough free for the system, so decided to reduce memory usage.

 - Walking '1's test - Data bus stress test (rotating single set bit pattern)
 - Pattern tests - Fills mem with 0xAA and 0x55
*/


void memtest(void)
{
	unsigned long x, mem_size=(1<<4);
	unsigned int rv;
	unsigned int *mem_ptr;
	int loops;

	// Print some info about variable sizes
	printf("Info: Sizes: char = %d, int = %d, long = %d\n",
				1 * sizeof(char),
				1 * sizeof(int),
				1 * sizeof(long));

	printf("Finding the biggest chunk of memory...\n");
	while(1){
		if((mem_ptr = malloc(mem_size * sizeof(int))) == NULL){
			break;
			}
		free(mem_ptr);
		mem_size += (1<<20); // 1MB at a time
		}
		// maximum mem found - decrease by say 4MB
		printf("Malloc eventually failed at %d ints\n", mem_size);

		// TODO shouldn't assume >4MB!!
		if(mem_size > 4*(1<<20)){
			mem_size = mem_size - 4*(1<<20); // 4MB less
			}

		if((mem_ptr = malloc(mem_size * sizeof (int)))==NULL){
			printf("ERROR - Malloc failed at %d ints!!\n", mem_size);
			return; // exit
			}

		printf("Allocated %d ints (0x%X to 0x%X)\n",
			mem_size, mem_ptr, (mem_ptr+mem_size-1));


		int test_val=0xAAAAAAAA;
		printf("\nFill Test - 0x%X\n", test_val);
		for(loops=1; loops<=3; loops++){
			printf(" - Pass %d\n", loops);
			memset(mem_ptr, test_val, mem_size * sizeof(int)); // fill mem 
//			printf("DEBUG - 0x%X=0x%X\n"
//				, mem_ptr+mem_size-2, mem_ptr[mem_size-1]);
//			printf("DEBUG - 0x%X=0x%X\n"
//				, mem_ptr+mem_size-1, mem_ptr[mem_size-1]);


			for(x=0; x<mem_size; x++){

				if((rv = (mem_ptr[x])) != test_val){
				  printf("Memory error at 0x%X", mem_ptr+x);
				  printf(" - wrote 0x%X, Read 0x%X\n",
					test_val, rv);
					}

			//printf("DEBUG - %X, %X\n", mem_ptr+x, rv);
			mem_ptr[x]=~test_val; //clear tested location:
			//printf("0x%X\n",mem_ptr[x]);
			}
		}


		test_val=0x55555555;
		printf("\nFill Test - 0x%X\n", test_val);
		for(loops=1; loops<=3; loops++){
			printf(" - Pass %d\n", loops);
			memset(mem_ptr, test_val, mem_size * sizeof(int)); 
			for(x=0; x<mem_size; x++){
				if((rv = (mem_ptr[x])) != test_val){
				  printf("Memory error at 0x%X",mem_ptr+x);
				  printf(" - wrote 0x%X, Read 0x%X\n",
					test_val, rv);
					}
			mem_ptr[x]=~test_val; //clear tested location:
			}
		}


		memTestDataBus(mem_ptr, mem_size);
//		printf("DEBUG - mem_ptr=0x%X, size=0x%X\n", mem_ptr, mem_size);

		printf("Memory Tests Finished\n");
		free(mem_ptr);
		return;
}

/*
// allocate a big chunk of memory and write some values 
void memtest_max(void)
{
	unsigned long x, mem_size=1<<4;
	unsigned int rv;
	unsigned int *mem_ptr;
	int loops;


	printf("Sizes: char = %d, int = %d, long = %d\n",
				1 * sizeof(char),
				1 * sizeof(int),
				1 * sizeof(long));

	printf("Finding the biggest chunk of memory...\n");
	while(1){
		if((mem_ptr = malloc(mem_size * sizeof(int))) == NULL){
			break;
			}
		free(mem_ptr);
		mem_size += (1<<20); // 1MB at a time
		}
		// maximum mem found - decrease by say 4MB
		printf("Malloc eventually failed at %d ints\n", mem_size);

		// TODO shouldn't assume >4MB!!
		mem_size = mem_size - 4*(1<<20); // 4MB less

		if((mem_ptr = malloc(mem_size * sizeof (int)))==NULL){
			printf("Malloc failed at %d ints!!\n", mem_size);
			return; // exit
			}

		printf("Got %d ints (0x%X to 0x%X)\n",
			mem_size, mem_ptr, (mem_ptr+mem_size-1));


		// Using curses to get into cbreak mode
		if((initscr())==NULL){
		    exit(EXIT_FAILURE);
		    }

		if(has_colors()){
			if(start_color() == ERR)
				printf("start_color failed\n");
		}

		addch('Y'|COLOR_RED);
		cbreak();
		memTestDataBus(mem_ptr, mem_size);
		nocbreak();
    		endwin();


		for(loops=1; loops<=3; loops++){
			printf("Filling with 0xAA's - Pass %d\n", loops);
			memset(mem_ptr, 0xAAAA, mem_size); // fill mem 
			for(x=0; x<mem_size; x++){
				if((rv = (mem_ptr[x])) != 0xAAAA){
				  printf("Memory error at 0x%X");
				  printf(" - wrote 0xAAAA, Read 0x%X\n",
						mem_ptr+x, rv);
					}
			mem_ptr[x]=0; //clear tested location:
			}
		}

		for(loops=1; loops<=3; loops++){
			printf("Filling with 0x5555's - Pass %d\n", loops);
			memset(mem_ptr, 0x5555, mem_size); // fill mem 
			for(x=0; x<mem_size; x++){
				if((rv = (mem_ptr[x])) != 0x5555){
				  printf("Memory error at 0x%X");
				  printf(" - wrote 0x5555, Read 0x%X\n",
						mem_ptr+x, rv);
					}
			mem_ptr[x]=0; //clear tested location:
			}
		}

		printf("Memory Test Passed\n");
		free(mem_ptr);
		return;
}

*/


void set_all_motors(int *handle, int direction, int speed)
{
	set_motors(handle, m1, direction, speed);
	set_motors(handle, m2, direction, speed);
	set_motors(handle, m3, direction, speed);
	set_motors(handle, m4, direction, speed);
	return;
}

void print_motors(int *handle)
{
	printf(" [%5i] [%5i] [%5i] [%5i]\n", 
		read_motors(handle, m1),
		read_motors(handle, m2),
		read_motors(handle, m3),
		read_motors(handle, m4));
}

void rampmotors(int *handle, int dir, int start_spd, int end_spd, long period)
{
	int i;

	if (start_spd < end_spd){
		printf("Ramp UP to %d\n", end_spd);
		for (i=start_spd; i<end_spd; i=i+4){
			//printf ("%d\n ",i);
			set_all_motors(handle, dir, i);
			usleep(period); // in microseconds
			}
	}
	else if (start_spd > end_spd){
		printf("Ramp DOWN to %d\n", end_spd);
		for (i=start_spd; i>end_spd; i=i-4){
			//printf ("%d\n ",i);
			set_all_motors(handle, dir, i);
			usleep(period); // in microseconds
			}
	}
	//return;
}

void motortest(int *handle)
{
	printf("Current speeds\n"); //print current motor status
	printf("    M1      M2      M3      M4\n");

	print_motors(handle);
	set_all_motors(handle, stop, 0);
	printf("Stopping motors\n");
	sleep(1);

	printf("Ramping up/down all motors\n");
	rampmotors(handle, fwd, 0, 255, 10000);
	sleep(1);
	rampmotors(handle, fwd, 255, 0, 10000);

/*
	printf("Max speed forward for 3 secs\n");
	set_all_motors(handle, fwd, 50);
	sleep(1);
	set_all_motors(handle, fwd, 255);
	sleep(3);
	set_all_motors(handle, stop, 10);
	sleep(3);
*/
	printf("Slow speed reverse\n");
	set_all_motors(handle, rev, 1);
	sleep(1);
	set_all_motors(handle, rev, 2);
	sleep(1);
	set_all_motors(handle, rev, 3);
	sleep(1);


	set_all_motors(handle, rev, 4);
	sleep(1);

	set_all_motors(handle, rev, 5);
	sleep(1);
	set_all_motors(handle, rev, 7);
	sleep(1);
	set_all_motors(handle, rev, 9);
	sleep(1);
	set_all_motors(handle, rev, 12);
	sleep(1);
	set_all_motors(handle, rev, 15);
	sleep(1);
	set_all_motors(handle, rev, 50);
	sleep(2);

	set_all_motors(handle, stop, 5);
	printf("Switching motors on individually\n");

	sleep(1);

	printf("Motor 1\n");
	set_motors(handle, m1, fwd, 200);
	sleep(1);
	set_motors(handle, m1, stop, 0);
	printf("Motor 2\n");
	set_motors(handle, m2, fwd, 200);
	sleep(1);
	set_motors(handle, m2, stop, 0);
	sleep(1);
	printf("Motor 3\n");
	set_motors(handle, m3, fwd, 200);
	sleep(1);
	set_motors(handle, m3, stop, 0);
	sleep(1);
	printf("Motor 4\n");
	set_motors(handle, m4, fwd, 200);
	sleep(1);
	set_motors(handle, m4, stop, 0);


	printf("All motors stopped\n");
/*
	rampmotor(handle, m1);
	rampmotor(handle, m2);
	rampmotor(handle, m3);
	rampmotor(handle, m4);
	write_all_motors(handle, stop, 0);
*/
}

void adctest(int *handle)
{
	printf("CH1 CH2 CH3 CH4\n");

	while(1){
		printf("%3i %3i %3i %3i\n",
			 read_8591(handle, ch1),
			 read_8591(handle, ch2),
			 read_8591(handle, ch3),
			 read_8591(handle, ch4));
		sleep(1);
		}
}

void ttltest(int *handle)
{
//	write_8574(handle, led1);
//	write_8574(handle, led4);
//	printf("led 1 + 4 should now be on\n");

	printf("All pins OFF\n");
	write_8574(handle, 0x00);
	sleep(2);	
	printf("All pins ON\n");
	write_8574(handle, 0xFF);
//	sleep(1);
//	write_8574(0x00);
	
//	printf("sw1 = [%i]\n", read_8574(handle, sw1));
//	printf("sw2 = [%i]\n", read_8574(handle, sw2));
//	printf("sw3 = [%i]\n", read_8574(handle, sw3));
//	printf("sw4 = [%i]\n", read_8574(handle, sw4));
}

void aboard_print(int *handle)
{
//	printf("[%4i][%4i][%4i][%4i][%4i][%4i][%4i]  [%4i][%4i][%4i][%4i]\n",
	printf("%4i %4i %4i %4i %4i %4i %4i   %4i %4i %4i %4i\n",
			read_aboard_sensor(handle, sen0),
			read_aboard_sensor(handle, sen1),
			read_aboard_sensor(handle, sen2),
			read_aboard_sensor(handle, sen3),
			read_aboard_sensor(handle, sen4),
			read_aboard_sensor(handle, sen5),
			read_aboard_sensor(handle, sen6),
			read_aboard_rotation(handle, sen0),
			read_aboard_rotation(handle, sen1),
			read_aboard_rotation(handle, sen2),
			read_aboard_rotation(handle, sen3));
}

void pboard_print(int *handle)
{
	printf("%4i %4i %4i %4i %4i %4i %4i\n",
			read_pboard_sensor(handle, sen0),
			read_pboard_sensor(handle, sen1),
			read_pboard_sensor(handle, sen2),
			read_pboard_sensor(handle, sen3),
			read_pboard_sensor(handle, sen4),
			read_pboard_sensor(handle, sen5),
			read_pboard_sensor(handle, sen6));
}

void aboardtest(int *handle)
{
	int ret;

	printf("Active Board Address = 0x08\n");
	//printf(" SEN0  SEN1  SEN2  SEN3  SEN4  SEN5  SEN6    ROT0  ROT1  ROT2  ROT3\n");
	printf("SEN0 SEN1 SEN2 SEN3 SEN4 SEN5 SEN6   ROT0 ROT1 ROT2 ROT3\n");
	printf("---- ---- ---- ---- ---- ---- ----   ---- ---- ---- ----\n");
	while(1){
		aboard_print(handle);
		sleep(1);
		}
	ret=0;
}


void pboardtest(int *handle)
{
	int ret;

	printf("Passive Board Address = 0x10\n");
	printf("SEN0 SEN1 SEN2 SEN3 SEN4 SEN5 SEN6\n");
	printf("---- ---- ---- ---- ---- ---- ----\n");

	while(1){
		pboard_print(handle);
		sleep(1);
		}
	ret=0;

// How about seperate tests for I/O?
//	set_pboard_IO(0x0f); // set IO header pins to input
//	printf("I/O header read[%x]\n",read_pboard_IO());

//	set_pboard_IO(0x00); // set pins to output
//	write_pboard_IO(0x0F); // set all pins high
}



/*
New PIC light sensor board
*/
void lighttest(int *handle)
{
// bytes that can be written to LBOARD
// 0x80 - start conversion (fast - sensor is continually converting)
// 0x90 - start conversion (slower - sensor is powered down after each reading)
// 0x00 - nop
// 0xA0 - Switch OFF I2C activity LED
// 0xA1 - Switch ON  I2C activity LED

	int ret, sen0, sen1, i;
	//int current, previous;
//	printf("Hertz=%i\n",ret*HZ);
	printf("Light Sensor - Looped Test\n");
	// sensor reading takes ~80ms (12.5Hz)

	printf("Flash Status LED a number of times\n");
	write_lboard_byte(handle, 0xA1); // activity LED on

	for(i=0; i<10; i++){
		write_lboard_byte(handle, 0);
		usleep(100000);
		}
	write_lboard_byte(handle, 0xA0); // acvitity LED off

	printf("Slow mode - Attempting 100 sensor reads...\n");
//	printf("sync=%d\n",sync);
	for(i=0; i<100; i++){
		ret = read_lboard_sensors(handle,slow);
		sen0=(ret & 0x00FF);
		sen1=(ret >> 8);
		printf("%3i = %3i %3i\n",
			i+1, sen0, sen1);
		}


	printf("Fast mode - Attempting 100 sensor reads...\n");
//	printf("async=%d\n",async);
	for(i=0; i<100; i++){
		ret = read_lboard_sensors(handle,fast);
		sen0=(ret & 0x00FF);
		sen1=(ret >> 8);
		printf("%3i = %3i %3i\n",
			i+1, sen0, sen1);
		}


//	ret = read_light_sensor(handle, adc0);
//	printf("Raw: 0x%x Step: 0x%x Chord: 0x%x\n",
//		ret & 0x7f, ret & 0xf, (ret >> 4) & 0x7);
	printf("Readings Done\n");

	write_lboard_byte(handle, 0xA1); // activity LED on
	write_lboard_byte(handle, 0); // dummy write to force flash 
}



/*===============  M A I N   S T A R T S   H E R E  ============*/

int main(int argc, char *argv[])
{
	int handle;

	if ((handle = init_i2c()) <= 0) {
		fprintf(stderr, "failed to open\n");
		return -1;
	}

/*----------------*/
/* argc/argv test */
/*----------------*/
/*	printf("argc=%i\n",argc); // number of args
	printf("argv[0]=\"%s\"\n",argv[0]);
	printf("argv[1]=\"%s\"\n",argv[1]);
	printf("argv[2]=\"%s\"\n\n",argv[2]);
*/

	if(argc == 1) {
		printf("*** Try \"%s ?\" for test types ***\n\n",argv[0]+2);
		probe();
		return(0);
		}

	if (argc >= 2){
		if ((strcmp("?",argv[1]))==0){
			printf("Usage: %s <Test_Type>\n\n",argv[0]+2);
			printf("\taboard = active board test\n");
			printf("\tpboard = passive board test\n");
			printf("\tlight  = light sensor test\n");
			printf("\tmotor  = motor test\n");
			printf("\tttl    = PCF8574 TTL test\n");
			printf("\tadc    = PCF8591 ADC test\n");
			printf("\tmem    = memory test\n");
			printf("\n");
			return 0;
			}

		else if((strcmp("aboard",argv[1]))==0){
			//printf("Doing active board test\n\n");
			aboardtest(&handle);
			return 0;
			}

		else if((strcmp("pboard",argv[1]))==0){
			//printf("Doing passive board test\n\n");
			pboardtest(&handle);
			return 0;
			}

		else if((strcmp("light",argv[1]))==0){
			printf("Doing Light sensor test\n\n");
			lighttest(&handle);
			return 0;
			}

		else if((strcmp("motor",argv[1]))==0){
			printf("Doing Motor test\n\n");
			motortest(&handle);
			return 0;
			}

		else if((strcmp("ttl",argv[1]))==0){
			printf("Doing PCF8574 TTL test\n\n");
			ttltest(&handle);
			return 0;
			}
		else if((strcmp("adc",argv[1]))==0){
			printf("Doing PCF8591 ADC test\n\n");
			adctest(&handle);
			return 0;
			}
		else if((strcmp("mem",argv[1]))==0){
			printf("Doing memory test\n\n");
			memtest();
			return 0;
			}
		else if((strcmp("curses",argv[1]))==0){
			printf("Doing curses test\n\n");
			curses_test();
			return 0;
			}
		}
	exit_i2c(handle);
	return 0;
}
