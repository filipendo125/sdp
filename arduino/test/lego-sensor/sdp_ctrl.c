/**
 * @author Alistair John Strachan <alistair@devzero.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "sdp_ctrl.h"

// sensor boards
#define ABOARD		0x08	// RA7 jumper = NONE
#define PBOARD		0x10	// RA7 jumper = NONE
#define LBOARD		0x14
#define PCF8574		0x20
#define TSL2550		0x39
#define PCF8591		0x4a

#define ABOARD2		0x09	// RA7 jumper = PRESENT
#define PBOARD2		0x11	// RA7 jumper = PRESENT

// motor boards
#define MOTORMUX	0x5a

// passive specific
#define PBUSY		0x8000
#define PCLEAR		0x03FF

// linux i2c device interface
#define I2C_DEV		"/dev/i2c-0"

static int force_access_retry (int *handle, int dev)
{
	int i;

	for (i = 0; i < 10; i++) {
		// it worked; we can leave now
		if (ioctl(*handle, I2C_SLAVE_FORCE, dev) >= 0)
			return 0;

		// it didn't, tell the user about it
		error("There was a problem opening device %d!\n", dev);

		// close the device
		close(*handle);

		// wait a bit
		usleep(100000);

		// open it again
		if ((*handle = open(I2C_DEV, O_RDWR)) <= 0)
			error("Failed to reopen %s!\n", I2C_DEV);
	}

	// it failed ten times, we can't do anything
	return -1;
}

int init_i2c(void)
{
	int handle;

	if ((handle = open(I2C_DEV, O_RDWR)) <= 0) {
		error("Error opening i2c %s!\n", I2C_DEV);
		return handle;
	}

	return handle;
}

void exit_i2c(int handle)
{
	if (close(handle) < 0)
		error("Closing i2c failed!\n");
}

void probe(void)
{
	int handle, port;

	if((handle = init_i2c()) <= 0)
		return;

	printf(" Search on %s for connected I2C addresses:\n\n", I2C_DEV);

	/* Loop through all available 7-Bit addresses (0-127) */
	for (port = 0; port <= 127; port++) {
		if (ioctl(handle, I2C_SLAVE_FORCE, port) < 0) {
			printf("access error at       "
			       "i2c-address: 0x%02x (%d)\n", port, port);
		}

		if (i2c_smbus_write_byte(handle, port) >= 0) {
			printf("found i2c-chip at     "
			       "i2c-address: 0x%02x (%d)\n", port, port);
		}
	}

	printf("\n probe on %s finished\n\n", I2C_DEV);

	exit_i2c(handle);
}


int read_motors(int *handle, Motor motor)
{
	int retval;

	if (force_access_retry(handle, MOTORMUX) < 0)
		return -1;

	i2c_smbus_write_byte(*handle, motor);

	retval = i2c_smbus_read_byte(*handle);

	i2c_smbus_write_byte(*handle, motor + 1);

	return retval | (i2c_smbus_read_byte(*handle) << 8);
}

int set_motors(int *handle, Motor motor, MotorDir dir, int speed)
{
	if (force_access_retry(handle, MOTORMUX) < 0)
		return -1;

	i2c_smbus_write_word_data(*handle, motor, (speed << 8) | dir);

	return 0;
}

/**
 * Written from TAOS TLS2550 specification TAOS029E
 */
int read_light_sensor(int *handle, Adc adc)
{
	int ret;

	if (force_access_retry(handle, TSL2550) < 0)
		return -1;

	i2c_smbus_write_byte(*handle, 0x00);	// Power-down state
	i2c_smbus_write_byte(*handle, 0x03);	// Power-up state
	//i2c_smbus_write_byte(*handle, 0x18);	// Standard Range Mode / Reset
	i2c_smbus_write_byte(*handle, 0x1d);	// Extended Range Mode / Reset

	i2c_smbus_write_byte(*handle, adc);	// Read ADC channel 0

	while (1) {
		ret = i2c_smbus_read_byte(*handle);
		if (ret & 0x80)
			break;
	}

	return ret;
}



/*
 PIC -> dual TSL2550 light sensor board.
 commands accepted by light board.
 board supports up to two light sensors.
 Sending: 0x80 returns the latest ADC conversion - fast
          0x90 waits for the latest conversion and - slow
          0xA0 Disables I2C activity LED
          0xA1 Enables I2C activity LED (also enabled if no sensors detected)

On power up the LED lights for 
The LED acts as a st

If no sensors were detected onpower up, the LED will flash on I2C activity.
This can also be switched on and off by sending 0xA1 and 0xA0 respectively.
*/
int read_lboard_sensors(int *handle, Mode mode)
{
	int ret, timeout=0;

	if (force_access_retry(handle, LBOARD) < 0)
		return -1;

//	i2c_smbus_write_byte(*handle, 0xa1); //flash status LED on I2C activity
	i2c_smbus_write_byte(*handle, mode); // send convert command

	if (mode==slow){
		usleep(50000); //wait 50mS
		}

	while (1) {// sensor returns 0x8080 until conversion is complete
		ret = i2c_smbus_read_word_data(*handle, 0x00);
		timeout++;
		if ((ret & 0x8080)==0){
			break;
			}

		if(timeout > 200){
			error("LBOARD - Timeout!");
			return -1;
			}
	}
	return ret;
}

// Send byte to Light Sensor board
int	write_lboard_byte(int *handle, char i2c_byte){

	if (force_access_retry(handle, LBOARD) < 0)
		return -1;

	i2c_smbus_write_byte(*handle, i2c_byte);
	return(0);
}







int write_8574(int *handle, Led led)
{
	int status;

	if (force_access_retry(handle, PCF8574) < 0)
		return -1;

	status = i2c_smbus_read_byte(*handle) & 0x0F;
	status = status | led;

	return i2c_smbus_write_byte(*handle, status);
}

int read_8574(int *handle, Switch sw)
{
	int status;

	if (force_access_retry(handle, PCF8574) < 0)
		return -1;

	status = i2c_smbus_read_byte(*handle) & 0xF0;

	return ((status & sw) != 0) ? 1 : 0;
}

int read_8591(int *handle, Channel ch)
{
	int status;

	if (force_access_retry(handle, PCF8591) < 0)
		return -1;

	i2c_smbus_write_byte(*handle, ch);
	status = i2c_smbus_read_byte(*handle);

	return (status > 128) ? 1 : 0;
}

/**
 * Sensors[0-3] are powered from PP3 battery(9V)
 *	Use for rotation, and light sensors
 *
 * Sensors[4-6] are powered from Vcc(5V)
 *	Can also be used for light sensors, but readings
 *	will be different from sensors[0-3]
 *
 * Returns 10-bit sensor reading from Active Sensor board
 * With no sensor attached return val = 1023
 */
int read_aboard_sensor(int *handle, Sensor ch)
{
	int retval;

	if (force_access_retry(handle, ABOARD) < 0)
		return -1;

	retval = i2c_smbus_read_word_data(*handle, (ch | 0x80));

	return retval & PCLEAR; //clear sensor number from top nibble
}

/**
 * Returns rotation sensor count from Active Sensor board
 * Sensor gives 16 counts per revolution, and is sampled every 3mS
 * (333 times/sec). Returned count is 12-bits.
 */
int read_aboard_rotation(int *handle, Sensor ch)
{
	int retval;

	if (force_access_retry(handle, ABOARD) < 0)
		return -1;

	retval = i2c_smbus_read_word_data(*handle, (ch | 0x70));

	return retval & 0xfff; //clear previous position from top nibble
}

/**
 * Returns 10-bit sensor reading from Passive Sensor board
 * With no sensor attached return val = 1023
 */
int read_pboard_sensor(int *handle, Sensor ch)
{
	int limit = 10, retval;

	if (force_access_retry(handle, PBOARD) < 0)
		return -1;

	i2c_smbus_write_byte(*handle, (ch | 0x80));

	do {
		retval = i2c_smbus_read_word_data(*handle, 0x77);

		// don't loop forever please
		if (--limit == 0)
			break;
	} while(retval & PBUSY);

	//clear sensor number from top nibble
	return retval & PCLEAR;
}

/**
 * Set RBx pin direction - 1=input, 0=ouput
 * eg. 0x0F will set all RBx pins to input 
 *     0x01 will set RB[5,3,2] to output, and RB0 to input
 *
 * Note: Pins closest to the left and right hand edges of board
 *       are connected to GND. Not much problem when used as simple
 *       switch inputs, but please double check connections when
 *       using as outputs.
 */
int set_pboard_IO(int *handle, int pins)
{
	if (force_access_retry(handle, PBOARD) < 0){
		return -1;
		}
	i2c_smbus_write_byte(*handle, (0x50 | (pins & 0x0F)));
	i2c_smbus_read_byte(*handle); // dummy read

	return 0;
}

/**
 * Get status of RAx and RBx IO header pins
 * Return val - Bits 5-0 = RA6,RA5,RB5,RB3,RB2,RB0
 * Note: RAx pins are input only.
 *       Pins closest to the left and right hand side of board
 *       are connected to gnd. Not much problem when using as
 *       switch inputs, but double check connections when using
 *       as outputs.
 */
int read_pboard_IO(int *handle)
{
	if (force_access_retry(handle, PBOARD) < 0){
		return -1;
		}
	i2c_smbus_write_byte(*handle, 0x40);

	return i2c_smbus_read_byte(*handle);
}

/**
 * Write 4-bit value(lower nibble) to RB[5,3,2,0] pins
 * eg. 0x00 will set all pins low (assuming direction set to output)
 *     and 0x09 will set RB[5,0] high, and RB[3,2] low.
 */
int write_pboard_IO(int *handle, int pins)
{
	if (force_access_retry(handle, PBOARD) < 0){
		return -1;
		}
	i2c_smbus_write_byte(*handle, (0x60 | (pins & 0x0F)));
	i2c_smbus_read_byte(*handle); // dummy read

	return 0;

}
