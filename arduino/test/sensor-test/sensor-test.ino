#include "SDPArduino.h"
#include <Wire.h>

int loopcount = 0;
int addressIndex = 0;
char lastCommand = 0;
int validAddress[128];
int validAddressCount = 0;
int checking = 1;
int bytesRequested = 6;
int addressToBits = 20;

void setup() {
  SDPsetup();
  setIRSensor();
}

void loop() {
  
  Serial.println();
  loopcount = 0;
//  Serial.print(bytesRequested);
//  Serial.print(" bytesRequested:\t");

  Wire.requestFrom(addressToBits, bytesRequested);
  
  for (int i = 0; i < bytesRequested; i++){
    int b;
    b = (int8_t) Wire.read();
//    Serial.print(b);
//    Serial.print("\t");
    
    Serial.print("in bits:\t");
    for (int i = 0; i < 8; i++){
      int bit = (b >> i) & 1;
      bit = bit ^ 1;
      Serial.print(bit);
      Serial.print("\t");
    }
    Serial.println();
    delay(50)
    
  }
  
  loopcount++;
  delay(10+5*bytesRequested);


/*
  Wire.requestFrom(addressToBits, bytesRequested);
  int b = (int8_t) Wire.read();
  if (b!=0){
    Serial.println(b);
  }
*/

}

void setIRSensor(){
  // 0x00 Power-down state
  // 0x03 Power-up state/Read command register
  // 0x1D Write command to assert extended range mode
  // 0x18 Write command to reset or return to standard range mode
  // 0x43 Read ADC channel 0
  // 0x83 Read ADC channel 1
  Wire.beginTransmission(57);
  Wire.write(0x43);
  delay(1000);
  Wire.write(0x18);
  delay(1000);
  Wire.write(0x03);
  delay(1000);
  int i = 0;
  /*
  while(1){
    if (i%8 == 0){
      Serial.println();  
    }
    Wire.requestFrom(57, 1);
    while(Wire.available()){
      int b = (int8_t) Wire.read();
      Serial.print(b);    
      Serial.print("\t");
      i++;
      delay(10);
    }
  }
  */
  Wire.endTransmission();
  
  delay(500);
}


void encodertest(int motorNum,int d){
    // 0 == d backwards
    // 1 == d forwards
    Serial.println();
    Serial.print("motor ");
    Serial.print(motorNum);
    Serial.println(" forward");
    if (d){
      motorForward(motorNum,100);
    }
    else{
      motorBackward(motorNum,100);
    }
    delay(500);
    motorStop(motorNum);
}

void setAddress(int addressNum){
    addressToBits = addressNum;
    Serial.println();
    Serial.println();
    Serial.print("Listening to address: ");
    Serial.print(addressToBits);
    Serial.println();
}


void serialEvent() {
/*
  for (int i = 0; i < 100; i++) {
    Serial.print('#');
  }
  Serial.println();
  Serial.print("received: ");
*/
  
  char command = Serial.read();

//  Serial.println(command);

  if (command == '0'){setAddress(39);}
  else if (command == '1'){setAddress(57);}
  else if (command == '2'){setAddress(16);}
  else if (command == '3'){setAddress(20);}
  else if (command == '4'){setAddress(4);}
  else if (command == '5'){setAddress(5);}
  else if (command == 'b'){}
  else if (command == 'q'){encodertest(0,0);}
  else if (command == 'w'){encodertest(1,0);}
  else if (command == 'e'){encodertest(2,0);}
  else if (command == 'r'){encodertest(3,0);}
  else if (command == 't'){encodertest(4,0);}
  else if (command == 'y'){encodertest(5,0);}
  else if (command == 'a'){encodertest(0,1);}
  else if (command == 's'){encodertest(1,1);}
  else if (command == 'd'){encodertest(2,1);}
  else if (command == 'f'){encodertest(3,1);}
  else if (command == 'g'){encodertest(4,1);}
  else if (command == 'h'){encodertest(5,1);}
  else if (command == 'x' || command == 'z') {
    Serial.println();
    Serial.println();
    int n = 0;
    int error;
    validAddressCount = 0;
    
    for(int address = 0; address < 128; address++ ){
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    // 4 = motor board
    // 5 = sensor board
    // 39 = on off sensor board
    
    
      Wire.beginTransmission(address);
      error = Wire.endTransmission();
      
      if(error == 0){
        Serial.print(error);
        Serial.print("\t");
        Serial.println(address);
        n++;
        validAddress[validAddressCount] = address;
        validAddressCount++;
      }
      delay(5); 
    }
    Serial.print("Number of devices = ");
    Serial.println(n);

    delay(500); 
   if (validAddressCount > 0){
      Wire.requestFrom(addressToBits, bytesRequested);
      
      Serial.print("address: ");
      Serial.println(addressToBits);
      int b;
      // slave may send less than requested
      while(Wire.available()){
        // print the character
  
        b = (int8_t) Wire.read();
        Serial.print("in bytes:\t");
        Serial.println(b);
  
        // receive a byte as character
        if (command == 'z'){
          Serial.print("in bits:\t");
          for (int i = 0; i < 8; i++){
            int bit = (b >> i) & 1;
            bit = bit ^ 1;
            Serial.print(bit);
            Serial.print("\t");
          } 
        }
        Serial.println("");
        
        delay(10);
      }
      Serial.println("");
    }
  }
  delay(500); 
}
