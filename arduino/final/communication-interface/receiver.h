#ifndef HEADER_RECEIVER
  #define HEADER_RECEIVER
  
  #include "robot.h"

  #define RECEIVER_SEQUENCE_START 15 // the byte that resets the sequence state
  #define RECEIVER_SEQUENCE_0 0 // a sequence state
  #define RECEIVER_SEQUENCE_1 255 // the other, alternated sequence state
  
  #define AWAITING_SEQUENCE_BYTE 0 // the next byte received is expected to be the sequence byte
  #define AWAITING_COMMAND_BYTE 1 // the next byte received is expected to be a command byte
  #define AWAITING_CHECKSUM_BYTE 2 // the next byte received is expected to be a checksum byte
  
  class Receiver {
    public:
      Robot *robot;
      
      int communicationState; // which byte in the packet are we waiting on?
      boolean sequenceStateSet; // are we doing a handshake?
      byte sequenceState; // what sequence byte are we expecting
      // the three bytes of the packet
      byte sequenceByte;
      byte commandByte;
      byte checksumByte;

      // for multi-packet commands
      byte commandType;
      int currentDigit; // 4 digit command (eg t100) - numbered 4 3 2 1 from left to right
      int totalReceived;
      
      Receiver(Robot *theRobot);
      void byteReceived(byte receivedByte);
      void commandReceived(byte command);
  };
  
#endif
