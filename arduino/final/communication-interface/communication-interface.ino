#include <SDPArduino.h>
#include <Wire.h>
#include "robot.h"
#include "receiver.h"
#include "sender.h"
#include "queue.h"

Queue *sendQueue;
Sender *sender;
Robot *robot;
Receiver *receiver;

void setup() {
  sendQueue = new Queue();
  sender = new Sender(sendQueue);
  robot = new Robot(sender);
  receiver = new Receiver(robot);
}

void loop() {
  robot->robotLoop();
  sender->senderLoop();
  
//  unsigned long timestamp = millis();
//  if (timestamp % 1000 == 0) {
//    robot->printMotorPositions();
//  }
}

void serialEvent() {
  // when a byte is received
  byte receivedByte = Serial.read();
  // determine if this is an acknowledgment of a sent message, or a byte of a message to be received, and route the byte appropriately
  if (receivedByte == SENDER_SEQUENCE_START || receivedByte == SENDER_SEQUENCE_0 || receivedByte == SENDER_SEQUENCE_1) {
    sender->byteReceived(receivedByte);
  } else {
    receiver->byteReceived(receivedByte);
  }
}

