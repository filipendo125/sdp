#include "robot.h"

void Robot::moveRobot(int dir) {
  moving = true;
  moveRotations = -1; // keep moving until told to stop
  leftWheelDirection = dir;
  rightWheelDirection = dir;
  motorPositions[RIGHTWHEELPOSITION] = 0;
  motorPositions[LEFTWHEELPOSITION]= 0;
}

void Robot::moveRobotRotations(int dir, int rotations) {
  moveRobot(dir); 
  moveRotations = rotations; // move this many rotations
}

void Robot::turnRobot(int dir) {
  moving = true;
  moveRotations = -1; // keep moving until told to stop
  leftWheelDirection = dir;
  rightWheelDirection = !dir; // we are turning
  motorPositions[RIGHTWHEELPOSITION] = 0;
  motorPositions[LEFTWHEELPOSITION]= 0;
}

void Robot::turnRobotRotations(int dir, int rotations) {
  turnRobot(dir);
  moveRotations = rotations; // move this many rotations
}

