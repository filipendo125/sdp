#ifndef HEADER_ROBOT
  #define HEADER_ROBOT
  
  #include <Arduino.h>
  #include <SDPArduino.h>
  #include <Wire.h>
  #include "sender.h"
  
  #define ROTARY_COUNT 6
  #define ROTARY_SLAVE_ADDRESS 5
  
  #define FORWARD 0
  #define BACKWARD 1
  #define CLOCKWISE 0
  #define ANTICLOCKWISE 1
  
  #define LEFTWHEELMOTOR 1
  #define LEFTWHEELORIENTATION 1
  #define LEFTWHEELPOSITION 2
  
  #define RIGHTWHEELMOTOR 2
  #define RIGHTWHEELORIENTATION 1
  #define RIGHTWHEELPOSITION 0
  
  #define KICKERMOTOR 4
  #define KICKERORIENTATION 1
  #define KICKERPOSITION 1
  #define KICKING_TIMEOUT 500
  #define UNKICKING_TIMEOUT 500
  
  #define GRABBERMOTOR 3
  #define GRABBERORIENTATION 0
  
  #define GRABBING_TIMEOUT 200
  #define REBOUNDGRABBING_TIMEOUT 400 // TODO - increase to account for moving forward whilst grabbing
  #define UNGRABBING_TIMEOUT 500
  
  #define GRABBER_IDLE 0
  #define GRABBING 1
  #define REBOUND_GRABBING 2
  #define UNGRABBING 3
  
  #define IR_ADDRESS 57
  #define BALL_THRESHOLD -55
  
  #define DIGITAL_BOARD_ADDRESS 39
  #define LEFT_GRABBER_SENSOR 3
  #define LEFT_FRONT_SENSOR 1
  #define LEFT_SIDE_SENSOR 0
  #define RIGHT_GRABBER_SENSOR 4
  #define RIGHT_FRONT_SENSOR 7
  #define RIGHT_SIDE_SENSOR 6
  
  class Robot {
    public:
      Sender *sender;
      int motorPositions[ROTARY_COUNT];
      boolean moving;
      boolean leftWheelDirection;
      boolean rightWheelDirection;
      int moveRotations;
      
      boolean kicking;
      boolean unkicking;
      boolean unkicked;
      int maxKickPosition;
      unsigned long kickerTimestamp;
      
      int grabbingStatus;
      boolean grabbed;
      boolean ungrabbed;
      unsigned long grabberTimestamp;
      boolean sendMessageOnGrab;
      
      int8_t irValue;
      boolean haveBall;
      
      boolean frontCollision;
      boolean leftCollision;
      boolean rightCollision;
      
      Robot(Sender *theSender);
      void setupSensors();
      void setupKicker();
      void setupGrabber();
      void setupMotors();
      
      void robotLoop();
      void doSensors();
      int getTouchSensorValue(int8_t touchSensorValues, int touchSensorPosition);
      void doMotors();
      void doGrabber();
      void doKicker();
      
      void moveLeftWheel(int power, int dir);
      void moveRightWheel(int power, int dir);
      void moveLeftWheelForward(int power);
      void moveLeftWheelBackward(int power);
      void moveRightWheelForward(int power);
      void moveRightWheelBackward(int power);
      void moveKickerForward(int power);
      void moveKickerBackward(int power);
      void moveGrabberForward(int power);
      void moveGrabberBackward(int power);
      void halt();
      void haltAndSendMessage();
      
      void moveRobot(int dir);
      void moveRobotRotations(int dir, int rotations);
      void turnRobot(int dir);
      void turnRobotRotations(int dir, int rotations);
      
      void kickForward(int power);
      void kickBackward();
      void grab();
      void grabAndSendMessage();
      void grabHelper();
      void reboundGrab();
      void ungrab();

      void updateMotorPositions();
      void printMotorPositions();
  };
#endif

