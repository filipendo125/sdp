#include "robot.h"

Robot::Robot(Sender *theSender) {
  sender = theSender;
  memset(motorPositions, 0, sizeof(motorPositions));
  SDPsetup();
  setupSensors();
  setupMotors();
  setupKicker();
  setupGrabber();
}

void Robot::setupSensors() {
  ungrabbed = false;
  haveBall = false;
  
  // 0x00 Power-down state
  // 0x03 Power-up state/Read command register
  // 0x1D Write command to assert extended range mode
  // 0x18 Write command to reset or return to standard range mode
  // 0x43 Read ADC channel 0
  // 0x83 Read ADC channel 1
  Wire.beginTransmission(IR_ADDRESS); // 57 is address of IR sensor
  Wire.write(0x43);
  delay(1000); // can maybe be changed to 400ms
  Wire.write(0x18);
  delay(1000);
  Wire.write(0x03);
  delay(1000);
  Wire.endTransmission();
}

void Robot::setupMotors() {
  halt();
}

void Robot::setupKicker() {
  motorStop(KICKERMOTOR);
  kicking = false;
  unkicking = false;
  kickBackward(); // unkick by default
}

void Robot::setupGrabber() {
  motorStop(GRABBERMOTOR);
  grabbingStatus = GRABBER_IDLE;
  grab(); // grab by default
}


void Robot::robotLoop() {
  doSensors();
  doMotors();
  doGrabber();
  doKicker();
}

void Robot::doSensors() {
  updateMotorPositions();
    
  Wire.requestFrom(IR_ADDRESS, 1);
  irValue = (int8_t) Wire.read();
  
  if (irValue > BALL_THRESHOLD && !haveBall) {
    grab();
    haveBall = true;
    sender->sendByte('B');
  } else if (irValue <= BALL_THRESHOLD && haveBall) {
    haveBall = false;
    sender->sendByte('G');
  }
  
  Wire.requestFrom(DIGITAL_BOARD_ADDRESS, 1);
  int8_t touchSensorValues = (int8_t) Wire.read();
  
  int leftGrabberValue = getTouchSensorValue(touchSensorValues, LEFT_GRABBER_SENSOR);
  int rightGrabberValue = getTouchSensorValue(touchSensorValues, RIGHT_GRABBER_SENSOR);
  int leftFrontValue = getTouchSensorValue(touchSensorValues, LEFT_FRONT_SENSOR);
  int rightFrontValue = getTouchSensorValue(touchSensorValues, RIGHT_FRONT_SENSOR);
  int leftSideValue = getTouchSensorValue(touchSensorValues, LEFT_SIDE_SENSOR);
  int rightSideValue = getTouchSensorValue(touchSensorValues, RIGHT_SIDE_SENSOR);
  
  ungrabbed = leftGrabberValue == 1 || rightGrabberValue == 1;
  boolean previousFrontCollision = frontCollision;
  frontCollision = leftFrontValue == 1 || rightFrontValue == 1;
  boolean previousLeftCollision = leftCollision;
  leftCollision = leftSideValue == 1;
  boolean previousRightCollision = rightCollision;
  rightCollision = rightSideValue == 1;
  
  if (frontCollision && !previousFrontCollision) {
    sender->sendByte('F');
  }
  if (leftCollision && !previousLeftCollision) {
    sender->sendByte('L');
  }
  if (rightCollision && !previousRightCollision) {
    sender->sendByte('R');
  }
}

int Robot::getTouchSensorValue(int8_t touchSensorValues, int touchSensorPosition) {
  // touchSensorPosition is the value for a particular touch sensor, touch_value is 1 if it is pressed, 0 otherwise
  int touchSensorValue = (touchSensorValues >> touchSensorPosition) & 1;
  touchSensorValue = touchSensorValue ^ 1;
  return touchSensorValue;
}

void Robot::doMotors() {
  if (moving) {
    // if we are moving a set number of rotations, and have finished, stop and send the 'done' message
    if (moveRotations != -1 && (abs(motorPositions[LEFTWHEELPOSITION]) >= moveRotations && abs(motorPositions[RIGHTWHEELPOSITION]) >= moveRotations)) {
      haltAndSendMessage();
    }
  
    if ((frontCollision && leftWheelDirection == FORWARD && rightWheelDirection == FORWARD) || (rightCollision && leftWheelDirection == FORWARD && rightWheelDirection == BACKWARD) || (leftCollision && leftWheelDirection == BACKWARD && rightWheelDirection == FORWARD)) {
      halt();
    }
  }
  
  if (moving) {
    int fullMovePower = 100;
    int adjustedMovePower = 0.90 * fullMovePower;
    // if the left wheel has moved more than the right, reduce the left wheel's power
    if (abs(motorPositions[LEFTWHEELPOSITION]) > abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(adjustedMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    // if the right wheel has moved more than the left, reduce the right wheel's power
    } else if (abs(motorPositions[LEFTWHEELPOSITION]) < abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(adjustedMovePower, rightWheelDirection);
    // otherwise, keep going full power
    } else {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    }
  }
}

void Robot::doGrabber() {
  unsigned long timestamp = millis();
  if (grabbingStatus == GRABBING) {
    // keep grabbing until the timer runs out
    if (timestamp - grabberTimestamp >= GRABBING_TIMEOUT) {
      motorStop(GRABBERMOTOR);
      reboundGrab();
    }
  } else if (grabbingStatus == REBOUND_GRABBING) {
    // keep rebound grabbing until the timer runs out
    if (timestamp - grabberTimestamp >= REBOUNDGRABBING_TIMEOUT) {
      motorStop(GRABBERMOTOR);
      grabbed = true;
      grabbingStatus = GRABBER_IDLE;
      if (sendMessageOnGrab == true) {
        sender->sendByte('*');
        sendMessageOnGrab = false;
      }
    }
  } else if (grabbingStatus == UNGRABBING) {
    // keep ungrabbing until we are ungrabbed or a large timeout occurs
    if (ungrabbed || timestamp - grabberTimestamp >= UNGRABBING_TIMEOUT) {
      motorStop(GRABBERMOTOR);
      grabbed = false;
      grabbingStatus = GRABBER_IDLE;
    }
  } else if (haveBall && moving && leftWheelDirection != rightWheelDirection) {
      // grab a little whilst turning with the ball
      moveGrabberForward(20);
  } else {
    motorStop(GRABBERMOTOR);
  }
}

void Robot::doKicker() {
  unsigned long timestamp = millis();
  if (kicking) {
    // keep kicking until the kicker is fully kicked
    if (timestamp - kickerTimestamp >= KICKING_TIMEOUT) {
      motorStop(KICKERMOTOR);
      kickBackward();
    }
    unkicked = false;
  } else if (unkicking) {
    if (abs(motorPositions[KICKERPOSITION]) <= 5) {
      motorStop(KICKERMOTOR);
    } else {
      moveKickerBackward(80);
    }
    if (!grabbed && UNKICKING_TIMEOUT < timestamp - kickerTimestamp && timestamp - kickerTimestamp <= UNKICKING_TIMEOUT + 10)  {
      unkicked = true;
      grab();
    }
  }
}
