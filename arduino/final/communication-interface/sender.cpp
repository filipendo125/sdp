#include "sender.h"

Sender::Sender(Queue *theSendQueue) {
  sendQueue = theSendQueue;
  reset();
}

void Sender::reset() {
  clearSendQueue();
  sequenceByte = SENDER_SEQUENCE_START;
  latestSendTime = millis();
  
  // flush input buffer
  while (Serial.available() > 0) {
    Serial.read();
  }
  
  // flush output buffer
  Serial.flush();
}

void Sender::sendByte(byte theByte) {
  if (sendQueue->count() > 20) {
    clearSendQueue();
  }
  if (sendQueue->isEmpty()) {
    sendQueue->push(theByte);
  } else {
    if (sendQueue->peek() != theByte) {
      sendQueue->push(theByte);
    }
  }
}

void Sender::senderLoop() {
  // send the command to be sent, every RETRANSMISSION_TIMEOUT seconds, until acknowledged
  if (!sendQueue->isEmpty() && millis() - latestSendTime >= RETRANSMISSION_TIMEOUT) {
    byte byteToSend = sendQueue->peek();
    byte checksumByte = ~byteToSend;  // calculate the checksum byte such that byteToSend + checksumByte = 255
    Serial.write(sequenceByte);
    Serial.write(byteToSend);
    Serial.write(checksumByte);
    latestSendTime = millis();
  }  
}

void Sender::byteReceived(byte receivedByte) {
  if (!sendQueue->isEmpty() && receivedByte == sequenceByte) { // the sequence byte received matches the one just sent
    sendQueue->pop();
    // alternate the sequence byte
    if (sequenceByte == SENDER_SEQUENCE_START || sequenceByte == SENDER_SEQUENCE_1) {
      sequenceByte = SENDER_SEQUENCE_0;
    } else if (sequenceByte == SENDER_SEQUENCE_0) {
      sequenceByte = SENDER_SEQUENCE_1;
    }
  }
}

void Sender::clearSendQueue() {
  while (!sendQueue->isEmpty()) {
    sendQueue->pop();
  }
}
