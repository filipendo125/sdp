#include "robot.h"

void Robot::updateMotorPositions() {
  // Request motor position deltas from rotary slave board
  Wire.requestFrom(ROTARY_SLAVE_ADDRESS, ROTARY_COUNT);
  
  // Update the recorded motor positions
  for (int i = 0; i < ROTARY_COUNT; i++) {
    motorPositions[i] += (int8_t) Wire.read();  // Must cast to signed 8-bit type
  }
//  if (motorPosition[KICKERPOSITION] > 0) {
//    motorPosition[KICKERPOSITION] = 0;
//  }
}

void Robot::printMotorPositions() {
  Serial.print("0: ");
  Serial.print(motorPositions[0]);
  
  Serial.print(", 1: ");
  Serial.print(motorPositions[1]);
  
  Serial.print(", 2: ");
  Serial.print(motorPositions[2]);
  
  Serial.print(", 3: ");
  Serial.print(motorPositions[3]);
  
  Serial.print(", 4: ");
  Serial.print(motorPositions[4]);
  
  Serial.print(", 5: ");
  Serial.println(motorPositions[5]);
}
