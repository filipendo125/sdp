#ifndef HEADER_SENDER
  #define HEADER_SENDER
  
  #include <Arduino.h>
  #include "queue.h"
  
  #define SENDER_SEQUENCE_START 240 // the byte that resets the sequence state
  #define SENDER_SEQUENCE_0 85 // a sequence state
  #define SENDER_SEQUENCE_1 170 // the other, alternated sequence state
  
  #define RETRANSMISSION_TIMEOUT 1000 // in milliseconds
  
  class Sender {
    public:
      Queue *sendQueue;
      byte sequenceByte; // the sequence number of packet to send
      unsigned long latestSendTime; // when was the last time we sent it?
    
      Sender(Queue *theSendQueue);
      void reset();
      void senderLoop();
      void sendByte(byte theByte);
      void byteReceived(byte receivedByte);
      void clearSendQueue();
  };
  
#endif
