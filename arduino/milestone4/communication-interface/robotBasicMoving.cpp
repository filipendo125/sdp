#include "robot.h"

void Robot::moveLeftWheel(int power, int dir) {
  if (dir == FORWARD) {
    moveLeftWheelForward(power);
  } else if (dir == BACKWARD) {
    moveLeftWheelBackward(power);
  }
}

void Robot::moveRightWheel(int power, int dir) {
  if (dir == FORWARD) {
    moveRightWheelForward(power);
  } else if (dir == BACKWARD) {
    moveRightWheelBackward(power);
  }
}

void Robot::moveLeftWheelForward(int power) {
  if (LEFTWHEELORIENTATION == 0) {
    motorForward(LEFTWHEELMOTOR, power);
  } else if (LEFTWHEELORIENTATION == 1) {
    motorBackward(LEFTWHEELMOTOR, power);
  }
}

void Robot::moveLeftWheelBackward(int power) {
  if (LEFTWHEELORIENTATION == 0) {
    motorBackward(LEFTWHEELMOTOR, power);
  } else if (LEFTWHEELORIENTATION == 1) {
    motorForward(LEFTWHEELMOTOR, power);
  }
}

void Robot::moveRightWheelForward(int power) {
  if (RIGHTWHEELORIENTATION == 0) {
    motorForward(RIGHTWHEELMOTOR, power);
  } else if (RIGHTWHEELORIENTATION == 1) {
    motorBackward(RIGHTWHEELMOTOR, power);
  }
}

void Robot::moveRightWheelBackward(int power) {
  if (RIGHTWHEELORIENTATION == 0) {
    motorBackward(RIGHTWHEELMOTOR, power);
  } else if (RIGHTWHEELORIENTATION == 1) {
    motorForward(RIGHTWHEELMOTOR, power);
  }
}

void Robot::moveKickerForward(int power) {
  if (KICKERORIENTATION == 0) {
    motorForward(KICKERMOTOR, power);
  } else if (KICKERORIENTATION == 1) {
    motorBackward(KICKERMOTOR, power);
  }
}

void Robot::moveKickerBackward(int power) {
  if (KICKERORIENTATION == 0) {
    motorBackward(KICKERMOTOR, power);
  } else if (KICKERORIENTATION == 1) {
    motorForward(KICKERMOTOR, power);
  }
}

void Robot::moveGrabberForward(int power) {
  if (GRABBERORIENTATION == 0) {
    motorForward(GRABBERMOTOR, power);
  } else if (GRABBERORIENTATION == 1) {
    motorBackward(GRABBERMOTOR, 100);
  }
}

void Robot::moveGrabberBackward(int power) {
  if (GRABBERORIENTATION == 0) {
    motorBackward(GRABBERMOTOR, power);
  } else if (GRABBERORIENTATION == 1) {
    motorForward(GRABBERMOTOR, power);
  }
}

void Robot::halt() {
  motorStop(LEFTWHEELMOTOR);
  motorStop(RIGHTWHEELMOTOR);
  if (haveBall && moving && leftWheelDirection != rightWheelDirection) {
    grab();
  }
  moving = false;
}

void Robot::haltAndSendMessage() {
  motorStop(LEFTWHEELMOTOR);
  motorStop(RIGHTWHEELMOTOR);
  if (haveBall && moving && leftWheelDirection != rightWheelDirection) {
    grabAndSendMessage();
  } else {
    sender->sendByte('*');
  }
  moving = false;
}
