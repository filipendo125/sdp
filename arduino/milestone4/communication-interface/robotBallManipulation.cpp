#include "robot.h"

void Robot::kickForward(int power) {
  ungrab();
  moveKickerForward(power);
  kicking = true;
  unkicking = false;
  kickerTimestamp = millis();
}

void Robot::kickBackward() {
  maxKickPosition = motorPositions[KICKERPOSITION];
  moveKickerBackward(80);
  kicking = false;
  unkicking = true;
  kickerTimestamp = millis();
}

void Robot::grab() {
  sendMessageOnGrab = false;
  grabHelper();
}

void Robot::grabAndSendMessage() {
  sendMessageOnGrab = true;
  grabHelper();
}

void Robot::grabHelper() {
  moveGrabberForward(100);
  grabbingStatus = GRABBING;
  grabberTimestamp = millis();
}

void Robot::reboundGrab() {
  moveGrabberForward(10);
  grabbingStatus = REBOUND_GRABBING;
  grabberTimestamp = millis();
}

void Robot::ungrab() {
  moveGrabberBackward(80);
  grabbingStatus = UNGRABBING;
  grabberTimestamp = millis();
}

