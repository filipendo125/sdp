#include "robot.h"

void Robot::robotLoop() {
  updateMotorPositions();
  doMotors();
  doGrabber();
  doKicker();
}

void Robot::doMotors() {
  int fullMovePower = 100;
  int adjustedMovePower = 90 * fullMovePower;
  if (moving) {
    if (abs(motorPositions[LEFTWHEELPOSITION]) > abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(adjustedMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    } else if (abs(motorPositions[LEFTWHEELPOSITION]) < abs(motorPositions[RIGHTWHEELPOSITION])) {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(adjustedMovePower, rightWheelDirection);
    } else {
      moveLeftWheel(fullMovePower, leftWheelDirection);
      moveRightWheel(fullMovePower, rightWheelDirection);
    }
  }
}

void Robot::doGrabber() {
  // TODO grabbing/ungrabbing timer
}

void Robot::doKicker() {
  // TODO timer similar to grabber
}
