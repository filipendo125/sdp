#include "robot.h"

void Robot::moveRobot(int dir) {
  moving = true;
  leftWheelDirection = dir;
  rightWheelDirection = dir;
  motorPositions[RIGHTWHEELPOSITION] = 0;
  motorPositions[LEFTWHEELPOSITION]= 0;
}

void Robot::moveRobotRotations(int dir, int rotations) {
//  moveSetup(dir, dir);
//  while (!Serial.available() && abs(motorPositions[RIGHTWHEELPOSITION]) < rotations && abs(motorPositions[LEFTWHEELPOSITION]) < rotations) {
//    moveLoopBody(dir, dir);
//  }
//  moveEnd();
}

void Robot::moveRobotCentimeters(int dir, int centimeters) {
  moveRobotRotations(dir, centimetersToRotations(centimeters));
}

void Robot::turnRobot(int dir) {
  moving = true;
  leftWheelDirection = dir;
  rightWheelDirection = !dir;
  motorPositions[RIGHTWHEELPOSITION] = 0;
  motorPositions[LEFTWHEELPOSITION]= 0;
}

void Robot::turnRobotRotations(int dir, int rotations) {
//  maintainGrab();
//  moveSetup(dir, !dir);
//  adjustedMovePower = 0.8 * fullMovePower;
//  while (!Serial.available() && abs(motorPositions[LEFTWHEELPOSITION]) < rotations && abs(motorPositions[RIGHTWHEELPOSITION]) < rotations) {
//    moveLoopBody(dir, !dir);
//  }
//  moveEnd();
//  motorStop(GRABBERMOTOR);
}

void Robot::turnRobotAngle(int dir, int angle) {
  turnRobotRotations(dir, angleToRotations(angle));
}

//void Robot::turnAntiClockwiseAngle(int power, int angle) {
//15 motor rotation is about 90 degrees
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!MOTOR ROTATION FOR CLOCKWISE AND ANTICLOCKWISE WILL BE DIFFERENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//Rotation varies each time. Robot will some time struggle part of the turn and increase motor count, even though it's not at the desired angle
//  //15 motor rotation is about 90 degrees
//  motorPositions[RIGHTWHEELPOSITION] = 0;
//  motorPositions[LEFTWHEELPOSITION] = 0;
//  moveRightWheelForward(power);
//  moveLeftWheelBackward(power);
//  while (abs(motorPositions[LEFTWHEELPOSITION]) < ((angle)/6)+((angle/6)*.1)) {
//    updateMotorPositions();  
//    Serial.println(motorPositions[LEFTWHEELPOSITION]);
//    if (abs(motorPositions[LEFTWHEELPOSITION]) < abs(motorPositions[RIGHTWHEELPOSITION])) {
//      moveRightWheelForward(power*0.83);
//      while (motorPositions[LEFTWHEELPOSITION] != motorPositions[RIGHTWHEELPOSITION]) {
//        updateMotorPositions();
//        Serial.println(motorPositions[LEFTWHEELPOSITION]);
//      }
//      moveRightWheelForward(power);
//    
//    } else if (abs(motorPositions[LEFTWHEELPOSITION]) > abs(motorPositions[RIGHTWHEELPOSITION])) {
//      moveLeftWheelBackward(power*0.83);
//      while (motorPositions[LEFTWHEELPOSITION] != motorPositions[RIGHTWHEELPOSITION]) {
//        updateMotorPositions();   
//        Serial.println(motorPositions[LEFTWHEELPOSITION]); 
//      }          
//      moveLeftWheelBackward(power);
//    }
//    
//    if (Serial.available()) {
//      break;
//    }
//  }
//  motorStop(LEFTWHEELMOTOR);
//  motorStop(RIGHTWHEELMOTOR);
//  maintainGrab();
//}

//void moveForwardDistance(int power, int distance) {
//  int curDist = 0;
//  motorPositions[RIGHTWHEELPOSITION] = 0;
//  motorPositions[LEFTWHEELPOSITION] = 0;
//  moveLeftWheelForward(power);
//  moveRightWheelForward(power);
//  while (curDist < distance) {
//    updateMotorPositions();
//    curDist = convertMotor(abs(motorPositions[RIGHTWHEELPOSITION]));
//    Serial.println(curDist);
//    if (abs(motorPositions[LEFTWHEELPOSITION]) > abs(motorPositions[RIGHTWHEELPOSITION])) {
//      moveLeftWheelForward(power*0.6);
//      // delay(3000);
//      updateMotorPositions();
//      while (abs(motorPositions[LEFTWHEELPOSITION]) > abs(motorPositions[RIGHTWHEELPOSITION]) && curDist < distance) { //added the second condition and it stopped the inifite loop. 
//        updateMotorPositions();
//        curDist = convertMotor(abs(motorPositions[RIGHTWHEELPOSITION]));
//        Serial.println(curDist+300);
//        if (Serial.available()) {
//          break;
//        }
//      }
//      moveLeftWheelForward(power);
//    
//    } else if (abs(motorPositions[LEFTWHEELPOSITION]) < abs(motorPositions[RIGHTWHEELPOSITION])) {
//      moveRightWheelForward(power*0.6);
//      //delay(3000);
//      updateMotorPositions();
//      while (abs(motorPositions[LEFTWHEELPOSITION]) < abs(motorPositions[RIGHTWHEELPOSITION]) && curDist < distance) {
//        updateMotorPositions();    
//        curDist = convertMotor(abs(motorPositions[RIGHTWHEELPOSITION])); 
//        Serial.println(curDist+100);
//        if (Serial.available()) {
//          break;
//        }
//      }          
//      moveRightWheelForward(power);
//    }
//    
//    else {
//        updateMotorPositions();
//    }
//  }
//  motorStop(LEFTWHEELMOTOR);
//  motorStop(RIGHTWHEELMOTOR);
//}

