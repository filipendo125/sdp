#include "robot.h"

void Robot::moveLeftWheel(int power, int dir) {
  if (dir == FORWARD) {
    moveLeftWheelForward(power);
  } else if (dir == BACKWARD) {
    moveLeftWheelBackward(power);
  }
}

void Robot::moveRightWheel(int power, int dir) {
  if (dir == FORWARD) {
    moveRightWheelForward(power);
  } else if (dir == BACKWARD) {
    moveRightWheelBackward(power);
  }
}

void Robot::moveLeftWheelForward(int power) {
  if (LEFTWHEELORIENTATION == 0) {
    motorForward(LEFTWHEELMOTOR, power);
  } else if (LEFTWHEELORIENTATION == 1) {
    motorBackward(LEFTWHEELMOTOR, power);
  }
}

void Robot::moveLeftWheelBackward(int power) {
  if (LEFTWHEELORIENTATION == 0) {
    motorBackward(LEFTWHEELMOTOR, power);
  } else if (LEFTWHEELORIENTATION == 1) {
    motorForward(LEFTWHEELMOTOR, power);
  }
}

void Robot::moveRightWheelForward(int power) {
  if (RIGHTWHEELORIENTATION == 0) {
    motorForward(RIGHTWHEELMOTOR, power);
  } else if (RIGHTWHEELORIENTATION == 1) {
    motorBackward(RIGHTWHEELMOTOR, power);
  }
}

void Robot::moveRightWheelBackward(int power) {
  if (RIGHTWHEELORIENTATION == 0) {
    motorBackward(RIGHTWHEELMOTOR, power);
  } else if (RIGHTWHEELORIENTATION == 1) {
    motorForward(RIGHTWHEELMOTOR, power);
  }
}

void Robot::halt() {
  motorStop(LEFTWHEELMOTOR);
  motorStop(RIGHTWHEELMOTOR);
  motorStop(GRABBERMOTOR); // to remove at some point
  motorStop(KICKERMOTOR); // to remove at some point
  moving = false;
  printMotorPositions();
}
