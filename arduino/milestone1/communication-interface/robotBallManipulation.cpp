#include "robot.h"

void Robot::kickForward(int power) {
  //ungrab();
  if (KICKERORIENTATION == 0) {
    motorForward(KICKERMOTOR, 100);
  } else if (KICKERORIENTATION == 1) {
    motorBackward(KICKERMOTOR, 100);
  }
  // TODO timer similar to grabber
}

void Robot::kickBackward() {
  if (KICKERORIENTATION == 0) {
    motorBackward(KICKERMOTOR, 100);
  } else if (KICKERORIENTATION == 1) {
    motorForward(KICKERMOTOR, 100);
  }
  // TODO timer similar to grabber
}

void Robot::grab() {
  kickBackward();
  grabbed = true;
  if (GRABBERORIENTATION == 0) {
    motorForward(GRABBERMOTOR, 100);
  } else if (GRABBERORIENTATION == 1) {
    motorBackward(GRABBERMOTOR, 100);
  }
  // TODO set timer to stop grabbing in robotLoop()
}

void Robot::ungrab() {
  grabbed = false;
  if (GRABBERORIENTATION == 0) {
    motorBackward(GRABBERMOTOR, 80);
  } else if (GRABBERORIENTATION == 1) {
    motorForward(GRABBERMOTOR, 80);
  }
  // TODO set timer to stop ungrabbing in robotLoop()
}

