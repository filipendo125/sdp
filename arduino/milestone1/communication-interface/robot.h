#ifndef HEADER_ROBOT
  #define HEADER_ROBOT
  
  #include <Arduino.h>
  #include <SDPArduino.h>
  #include <Wire.h>
  
  #define ROTARY_COUNT 6
  #define ROTARY_SLAVE_ADDRESS 5
  
  #define FORWARD 0
  #define BACKWARD 1
  #define CLOCKWISE 0
  #define ANTICLOCKWISE 1
  
  #define LEFTWHEELMOTOR 5
  #define LEFTWHEELORIENTATION 1
  #define LEFTWHEELPOSITION 1
  
  #define RIGHTWHEELMOTOR 2
  #define RIGHTWHEELORIENTATION 1
  #define RIGHTWHEELPOSITION 2
  
  #define KICKERMOTOR 3
  #define KICKERORIENTATION 1
  
  #define GRABBERMOTOR 4
  #define GRABBERORIENTATION 0
  
  class Robot {
    public:
      int motorPositions[ROTARY_COUNT];
      boolean moving;
      boolean leftWheelDirection;
      boolean rightWheelDirection;
      boolean grabbed;
      
      Robot();
      void setupKicker();
      void setupGrabber();
      
      void robotLoop();
      void doMotors();
      void doGrabber();
      void doKicker();
      
      void moveLeftWheel(int power, int dir);
      void moveRightWheel(int power, int dir);
      void moveLeftWheelForward(int power);
      void moveLeftWheelBackward(int power);
      void moveRightWheelForward(int power);
      void moveRightWheelBackward(int power);
      void halt();
      
      void moveRobot(int dir);
      void moveRobotRotations(int dir, int rotations);
      void moveRobotCentimeters(int dir, int centimeters);
      void turnRobot(int dir);
      void turnRobotRotations(int dir, int rotations);
      void turnRobotAngle(int dir, int angle);
      
      void kickForward(int power);
      void kickBackward();
      void grab();
      void ungrab();

      void updateMotorPositions();
      void printMotorPositions();
      int centimetersToRotations(int centimeters);
      int angleToRotations(int angle);
  };
#endif

