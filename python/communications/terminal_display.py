import sys
from time import time, strftime
import StringIO
import curses
import traceback

START_TIMEOUT = 1  # in seconds
RELATIVE_LOG_PATH = "display_log.txt"


class TerminalDisplay:
    def __init__(self, user_input_handler):
        self.running = False
        self.closed = True
        self.user_input_handler = user_input_handler

        self.write_to_display = True

        # get ready to capture standard output
        self.sys_stdout = sys.stdout
        self.stdout_buffer = StringIO.StringIO()
        self.stdout_pos = 0  # 1st read position of the stdout stream.

        # get ready to capture standard error
        self.sys_stderr = sys.stderr
        self.stderr_buffer = StringIO.StringIO()
        self.stderr_pos = 0  # 1st read position of the stdout stream.

        self.stdscr = None

        initial_log_message = "{}\nLOGGING STARTED {}\n".format("-" * 24, strftime("%H:%M:%S %a %d %b %Y"))
        self.log(initial_log_message)

    @staticmethod
    def log(message):
        with open(RELATIVE_LOG_PATH, "a+") as log_file:
            log_file.write(message)

    @staticmethod
    def print_and_log(message):
        print(message)
        TerminalDisplay.log(message)

    def wait_for_start(self):
        timeout = False
        timestamp = time()
        while not self.running and not timeout:
            if time() - timestamp >= START_TIMEOUT:
                timeout = True
                print("[LAUNCH] failed to start terminal display within {} seconds".format(START_TIMEOUT))
        return not timeout

    def run(self):
        self.closed = False

        # capture standard output and standard error
        sys.stdout = self.stdout_buffer
        sys.stderr = self.stderr_buffer

        # initialise curses window
        self.stdscr = curses.initscr()
        curses.noecho()
        self.stdscr.nodelay(1)

        self.running = True
        print("[LAUNCH] terminal display started")
        while not self.closed:
            # print and log captured standard output if available
            if self.stdout_buffer.tell() > self.stdout_pos:
                try:
                    # get the captured input
                    self.stdout_buffer.seek(self.stdout_pos)
                    captured_output = self.stdout_buffer.read()
                    self.stdout_pos = self.stdout_buffer.tell()

                    # write to log file
                    TerminalDisplay.log(captured_output)

                    if self.write_to_display:
                        # write to curses window
                        try:
                            self.stdscr.addstr(captured_output)
                        except curses.error:
                            # the window is full - clear and reset the cursor
                            self.stdscr.clear()
                            self.stdscr.move(0, 0)
                            self.stdscr.addstr(captured_output)
                except (curses.error, Exception) as e:
                    TerminalDisplay.print_and_log("[LAUNCH] error printing stdout '{}'\n".format(e.message))
                    traceback.print_exc()

            # print and log captured standard error if available
            if self.stderr_buffer.tell() > self.stderr_pos:
                try:
                    # get the captured input
                    self.stderr_buffer.seek(self.stderr_pos)
                    captured_output = self.stderr_buffer.read()
                    self.stderr_pos = self.stderr_buffer.tell()

                    # write to log file
                    TerminalDisplay.log(captured_output)

                    if self.write_to_display:
                        # write to curses window
                        try:
                            self.stdscr.addstr(captured_output)
                        except curses.error:
                            # the window is full - clear and reset the cursor
                            self.stdscr.clear()
                            self.stdscr.move(0, 0)
                            self.stdscr.addstr(captured_output)
                except (curses.error, Exception) as e:
                    TerminalDisplay.print_and_log("[LAUNCH] error printing stderr '{}'\n".format(e.message))
                    traceback.print_exc()

            # capture and handle user input
            try:
                c = self.stdscr.getch()
                if c != -1:
                    self.user_input_handler.handle_input(chr(c))
                    if c == ord('q'):
                        self.close()
                    elif c == ord('-'):
                        # manually clear the screen
                        self.stdscr.clear()
                        self.stdscr.move(0, 0)
                    elif c == ord('`'):
                        if self.write_to_display:
                            self.write_to_display = False
                        else:
                            self.write_to_display = True
            except (curses.error, Exception) as e:
                TerminalDisplay.print_and_log("[LAUNCH] error handling input '{}'\n".format(e))
                traceback.print_exc()

        self.running = False

    def close(self):
        if not self.closed:
            sys.stdout = self.sys_stdout  # return standard out to normal
            sys.stderr = self.sys_stderr  # return standard error to normal
            try:
                curses.echo()  # undo curses terminal options
                curses.endwin()  # exit curses
            except curses.error as e:
                TerminalDisplay.print_and_log("[LAUNCH] curses error: '{}'\n".format(e.message))
                TerminalDisplay.print_and_log("[LAUNCH] warning: if curses was open, it did not close gracefully.\n")
            TerminalDisplay.print_and_log("[LAUNCH] terminal display closed\n")
            self.closed = True

            closing_log_message = "LOGGING CLOSED {}\n{}\n\n".format(strftime("%H:%M:%S %a %d %b %Y"), "-" * 24)
            self.log(closing_log_message)
