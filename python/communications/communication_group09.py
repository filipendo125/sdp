import sys
from time import time
from threading import Thread
from collections import deque
from serial import Serial

RETRANSMISSION_TIMEOUT = 0.05
READ_TIMEOUT = 0
FAILURE_TIMEOUT = 5
MAX_QUEUE_LENGTH = 20


class CommunicationGroup09:
    def __init__(self, usb_port_number):
        """communication interface for sending commands and receiving sensor info to/from the Arduino

        :param usb_port_number: the USB port number for the SRF stick - normally either 1 or 0, found by finding X in
                the /dev/ttyACMX corresponding to the SRF stick (numbered by the order of USB devices connected)
        """
        self.closed = True
        self.connection = None
        self.sender = None
        self.receiver = None
        self.handle_message = None

        self.port = '/dev/ttyACM{}'.format(usb_port_number)
        self.baudrate = 115200
        self.read_timeout = READ_TIMEOUT

        self.router_thread = None
        self.router_thread_alive = False

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def open(self, handle_message):
        self.handle_message = handle_message

        print("[COMMS GROUP 9] opening connection...")
        self.router_thread = Thread(target=self.router, name="Communication Group 9 Thread")
        self.connection = Serial(port=self.port, baudrate=self.baudrate, timeout=self.read_timeout)
        self.sender = Sender(self.connection)
        self.receiver = Receiver(self.connection, self.handle_message)

        self.flush_input_buffer()
        self.connection.flush()

        self.closed = False
        self.router_thread_alive = True
        self.router_thread.start()

        # halt the robot by default
        self.halt()

    def flush_input_buffer(self):
        """get rid of all bytes waiting in the read buffer"""
        timeout = False
        while not timeout:
            response = self.connection.read(1)
            if response == '':
                timeout = True

    def router(self):
        """separate thread for routing received bytes to sender/receiver, and handling them appropriately"""
        while self.router_thread_alive:
            try:
                self.sender.sender_loop()  # for actually sending the messages in the sender's command queue
                received_byte = self.connection.read(1)  # read a byte to route (non blocking, as READ_TIMEOUT is 0)
                if received_byte == SENDER_SEQUENCE_START or received_byte == SENDER_SEQUENCE_0 or \
                        received_byte == SENDER_SEQUENCE_1:
                    self.sender.byte_received(received_byte)
                elif received_byte != '':
                    self.receiver.byte_received(received_byte)
            except (SenderException, Exception) as e:
                self.close(halt=False)
                raise e

    # shortcut functions for commands that can be sent
    def move_forward(self, rotations):
        self.send_command("t{}".format(self.int_to_three_digit_string(rotations)))

    def move_backward(self, rotations):
        self.send_command("g{}".format(self.int_to_three_digit_string(rotations)))

    def turn_clockwise(self, rotations):
        self.send_command("h{}".format(self.int_to_three_digit_string(rotations)))

    def turn_anticlockwise(self, rotations):
        self.send_command("f{}".format(self.int_to_three_digit_string(rotations)))

    def kick(self, power):
        self.send_command("b{}".format(power))

    def halt(self):
        self.send_command(" ")

    def full_kick(self):
        self.send_command("k")

    def grab(self):
        self.send_command("j")

    def ungrab(self):
        self.send_command("l")

    def unkick(self):
        self.send_command("i")

    def send_command(self, command_string):
        """send a string of commands (or a single command)
        :param command_string: the command string or character to send
        :type command_string: str
        """
        if not self.closed:
            print("[COMMS GROUP 9] sending command string '{}'".format(command_string))
            self.sender.send_command_string(command_string)
        else:
            print("[COMMS GROUP 9] command string '{}' not sent - connection not open!".format(command_string))

    def close(self, halt=True):
        """must be called to kill the router thread and exit cleanly"""
        if not self.closed:
            self.closed = True
            if halt:
                print("[COMMS GROUP 9] attempting to halt robot...")
                self.halt()  # in case the robot is still moving
                while self.sender.send_queue:  # wait for the halt command above to be acknowledged
                    pass
            print("[COMMS GROUP 9] closing connection...")
            self.router_thread_alive = False  # kill the router thread
            self.connection.close()  # kill the serial connection

    def restart(self):
        self.close(halt=False)
        self.open(self.handle_message)

    @staticmethod
    def int_to_three_digit_string(integer):
        """converts an integer into a three-digit string by adding leading zeros

        :param integer: the integer to convert
        :type integer: int
        :rtype: str
        """
        if 0 < integer < 999:
            if integer == 0:
                return '000'
            if integer < 10:
                return '00{}'.format(integer)
            elif integer < 100:
                return '0{}'.format(integer)
            else:
                return '{}'.format(integer)
        else:
            return '---'


# Sender constants - mirror the Receiver constants in the Arduino -> receiver.h
SENDER_SEQUENCE_START = chr(15)  # the byte that resets the sequence state
SENDER_SEQUENCE_0 = chr(0)  # a sequence state
SENDER_SEQUENCE_1 = chr(255)  # the other, alternated sequence state


class Sender:
    def __init__(self, connection):
        self.connection = connection
        self.sequence_byte = SENDER_SEQUENCE_START  # what sequence number are we on?
        self.send_queue = deque([])  # the commands to send, in order - pairs of command/checksum bytes
        self.latest_send_time = time()  # timestamp for when we last sent a packet
        self.latest_response = time()  # timestamp for when we last received a response

    def send_command_string(self, command_string):
        """send a string of commands

        :param command_string: the command string
        :type command_string: str
        """
        for command in command_string:
            self.send_byte(command)

    def send_byte(self, the_byte):
        """send a single command

        :param the_byte: the command to send
        :type the_byte: str - must be of length 1
        """
        checksum = chr(~ord(the_byte) + 128)  # byte + checksum = 127
        self.send_queue.append((the_byte, checksum))
        if len(self.send_queue) > MAX_QUEUE_LENGTH:
            sys.stderr.write("[COMMS GROUP 9] max send queue of {} exceeded, flushing...".format(MAX_QUEUE_LENGTH))
            self.send_queue = deque([])
        self.latest_response = time()

    def sender_loop(self):
        timestamp = time()
        # send the first command in the queue, every RETRANSMISSION_TIMEOUT seconds, until acknowledged
        if self.send_queue and timestamp - self.latest_send_time >= RETRANSMISSION_TIMEOUT:
            # raise an exception if we have not received a response after FAILURE_TIMEOUT seconds of expecting one
            if timestamp - self.latest_response >= FAILURE_TIMEOUT:
                self.send_queue = []
                raise SenderException("failure-timeout reached - no response within {} seconds"
                                      .format(FAILURE_TIMEOUT))
            # print("[COMMS GROUP 9] sending '{}'...\t".format(self.send_queue[0][0])),
            self.connection.write(self.sequence_byte)
            self.connection.write(self.send_queue[0][0])
            self.connection.write(self.send_queue[0][1])
            self.latest_send_time = time()

    def byte_received(self, received_byte):
        self.latest_response = time()
        if self.send_queue:
            if received_byte == self.sequence_byte:  # the sequence byte received matches the one just sent
                # print("acknowledged")
                self.send_queue.popleft()  # pop the send queue to start sending the next command in the queue
                self.alternate_sequence_byte()
            else:
                # print("invalid response '{}'".format(received_byte))
                pass

    def alternate_sequence_byte(self):
        if self.sequence_byte == SENDER_SEQUENCE_START or self.sequence_byte == SENDER_SEQUENCE_1:
            self.sequence_byte = SENDER_SEQUENCE_0
        elif self.sequence_byte == SENDER_SEQUENCE_0:
            self.sequence_byte = SENDER_SEQUENCE_1


class SenderException(Exception):
    pass


# Receiver constants - mirror the Sender constants in the Arduino -> sender.h
AWAITING_SEQUENCE_BYTE = 0  # the next byte received is expected to be the sequence byte
AWAITING_COMMAND_BYTE = 1  # the next byte received is expected to be a command byte
AWAITING_CHECKSUM_BYTE = 2  # the next byte received is expected to be a checksum byte

RECEIVER_SEQUENCE_START = chr(240)  # the byte that resets the sequence state
RECEIVER_SEQUENCE_0 = chr(85)  # a sequence state
RECEIVER_SEQUENCE_1 = chr(170)  # the other, alternated sequence state


class Receiver:
    def __init__(self, connection, handle_message):
        self.connection = connection
        self.handle_message = handle_message

        self.communication_state = AWAITING_SEQUENCE_BYTE  # which byte in the packet are we waiting on?
        self.sequence_state_set = False  # are we doing a handshake?
        self.sequence_state = RECEIVER_SEQUENCE_START  # what sequence byte are we expecting?
        # the three bytes of a packet
        self.sequence_byte = ''
        self.info_byte = ''
        self.checksum_byte = ''

    def byte_received(self, received_byte):
        # if the byte is the start of a message, get ready for the rest of the message
        if received_byte == RECEIVER_SEQUENCE_0 or received_byte == RECEIVER_SEQUENCE_1:
            self.sequence_byte = received_byte
            self.communication_state = AWAITING_COMMAND_BYTE

        # particularly, if the byte is the start of a 'handshake' message, the sequence state will need to be reset
        elif received_byte == RECEIVER_SEQUENCE_START:
            self.sequence_byte = received_byte
            self.communication_state = AWAITING_COMMAND_BYTE
            self.sequence_state_set = False

        # if we have just received the start of a message, then the next byte is the info byte, and the following
        # byte should be the checksum
        elif self.communication_state == AWAITING_COMMAND_BYTE:
            self.info_byte = received_byte
            self.communication_state = AWAITING_CHECKSUM_BYTE

        # if we get the checksum byte, process the complete message
        elif self.communication_state == AWAITING_CHECKSUM_BYTE:
            self.checksum_byte = received_byte
            self.communication_state = AWAITING_SEQUENCE_BYTE
            if ord(self.info_byte) + ord(self.checksum_byte) == 255:  # make sure the checksum checks out
                self.connection.write(self.sequence_byte)  # send the acknowledgement
                # it was a handshake message, so reset the sequence state and follow the command
                if not self.sequence_state_set:
                    self.sequence_state = RECEIVER_SEQUENCE_0
                    self.sequence_state_set = True
                    self.handle_message(self.info_byte)  # info is valid - handle it
                # only valid info if this message was not a duplicate
                elif self.sequence_byte == self.sequence_state:
                    # alternate the sequence state
                    if self.sequence_state == RECEIVER_SEQUENCE_0:
                        self.sequence_state = RECEIVER_SEQUENCE_1
                    elif self.sequence_state == RECEIVER_SEQUENCE_1:
                        self.sequence_state = RECEIVER_SEQUENCE_0
                    self.handle_message(self.info_byte)  # info is valid - handle it
