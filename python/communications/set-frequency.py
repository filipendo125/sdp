#!/usr/bin/env python

import sys
from time import sleep
from serial import Serial


class SrfStickCommunication:
    def __init__(self, usb_port_number):
        port = '/dev/ttyACM{}'.format(usb_port_number)
        baudrate = 115200
        timeout = 1

        print("opening connection...")
        self.connection = Serial(port=port, baudrate=baudrate, timeout=timeout)
        self.flush_input_buffer()
        self.connection.flush()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        print("closing connection...")
        self.connection.close()

    def flush_input_buffer(self):
        timeout = False
        while not timeout:
            response = self.connection.read(1)
            if response == '':
                timeout = True

    def set_frequency(self, frequency):
        if not self.send_srf_commands(["+++", "ATCN{}\r".format(frequency), "ATAC\r", "ATDN\r"]):
            sys.stderr.write("ERROR: Failed to set frequency.\n")
            self.close()

    def send_srf_commands(self, commands):
        for command in commands:
            print(command)
            if not self.send_srf_command(command):
                return False
        return True

    def send_srf_command(self, command):
        self.connection.write(command)
        self.connection.flush()

        response = self.connection.read(2)
        if not response:
            sleep(3)
            response = self.connection.read(2)

        self.connection.reset_input_buffer()
        if response == "OK":
            return True
        else:
            if command[-1] == '\r':
                command = command[:-1]
            sys.stderr.write("ERROR: command '{}' returned '{}'.\n".format(command, response))
            return False


class CommunicationException(Exception):
    pass


def main(usb_port_number):
    with SrfStickCommunication(usb_port_number) as com:
        com.set_frequency(50)

if __name__ == '__main__':
    try:
        usb_port_number = sys.argv[1]
    except IndexError:
        usb_port_number = 0
    main(usb_port_number)
