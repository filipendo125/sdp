import time as tm
from threading import Thread
from collections import deque
from serial import Serial
import sys


MAX_QUEUE_LENGTH = 20


class CommunicationGroup10:
    def __init__(self, usb_port_number):
        """communication interface for sending commands and receiving sensor info to/from the Arduino

        :param usb_port_number: the USB port number for the SRF stick - normally either 1 or 0, found by finding X in
                the /dev/ttyACMX corresponding to the SRF stick (numbered by the order of USB devices connected)
        """
        self.closed = True
        self.connection = None
        self.sender = None
        self.port = '/dev/ttyACM{}'.format(usb_port_number)
        self.baudrate = 115200
        self.has_ball = False
        self.timeout = 5

        # start the router thread
        self.router_thread = None
        self.router_thread_alive = False
        self.list = []

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def open(self, handle_message):
        self.handle_message = handle_message

        print("[COMMS GROUP 10] opening connection...")
        self.router_thread = Thread(target=self.router, name="Communication Group 10 Thread")
        self.connection = Serial(port=self.port, baudrate=self.baudrate, timeout=self.timeout)
        self.sender = Sender(self.connection)
        self.connection.flush()
        self.router_thread_alive = True
        self.router_thread.start()

        self.closed = False

    # shortcut functions for commands that can be sent
    def configure_command(self, bearing, heading, grabbing=0, kicking=0):
        self.list.append("{}{}{}{}".format(self.bearing_to_byte_string(bearing), self.bearing_to_byte_string(heading),
                                           chr(grabbing+33), chr(kicking+33)))

    @staticmethod
    def bearing_to_byte_string(angle):
        # range  0 to 180
        angle_one = int(angle/2)
        angle_two = angle - angle_one
        return "{}{}".format(chr(int(angle_one+33)), chr(angle_two+33))

    def router(self):
        """separate thread for routing received bytes to sender/receiver, and handling them appropriately"""
        while self.router_thread_alive:
            try:
                tm.sleep(0.1)
                d = None
                if len(self.list) > 0:
                    self.connection.write(self.list[-1])
                    self.list = self.list[:-1]
                d = self.connection.read(1)
                if d == 't':
                    self.handle_message("B")
                    print "[COMMS GROUP 10] ball acquired"
                else:
                    self.handle_message("G")
            except Exception as e:
                self.close()
                raise e

    def close(self):
        """must be called to kill the router thread and exit cleanly"""
        if not self.closed:
            self.closed = True
            print("[COMMS GROUP 10] closing connection")
            self.router_thread_alive = False  # kill the router thread
            self.connection.close()  # kill the serial connection

    def restart(self):
        self.close()
        self.open(self.handle_message)


class Sender:
    def __init__(self, connection):
        self.connection = connection
        self.send_queue = deque([])  # the commands to send, in order - pairs of command/checksum bytes

    def send_command_string(self, command_string):
        # print "Length of queue before is " + str(len(self.send_queue))
        self.send_queue.append(command_string)
        # print "Length of queue after is " + str(len(self.send_queue))
        if len(self.send_queue) > 0:
            sys.stderr.write("[COMMS GROUP 10] max send queue of {} exceeded, flushing...".format(MAX_QUEUE_LENGTH))
            last_value = self.send_queue[-1]
            self.send_queue = deque([last_value])

    def sender_loop(self):
        if self.send_queue:
            self.connection.write(self.send_queue[0])
            self.send_queue.pop()
