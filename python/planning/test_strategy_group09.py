class TestStrategyGroup09:
    def __init__(self, world_state, planner):
        self.quit = False
        self.launched = False
        self.world_state = world_state
        self.planner = planner

        self.task = "idle"
        self.subtask = None

    def close(self):
        self.quit = True

    def launch(self):
        print("[STRATEGY GROUP 9] launched")
        self.launched = True
        self.world_state.set_centre()
        self.world_state.set_goals()
        self.world_state.set_zones()
        self.world_state.set_receive_zones()
        self.world_state.update()
        self.planner.has_ball = False

        while not self.quit:
            if self.task == "idle":
                continue
            try:
                self.world_state.update()
                self.determine_subtask()
                self.planner.execution_step(self.subtask)
            except Exception as e:
                print(e.message)
        self.launched = False

    def determine_subtask(self):
        if self.task == "acquire ball":
            print(self.planner.subtask_complete("has ball"))
        elif self.task == "move to receive pass":
            print(self.planner.subtask_complete("move to receive pass"))

    def who_has_ball(self):
        return self.world_state.who_has_ball()

    def set_subtask(self, subtask, message):
        if self.subtask != subtask:
            self.subtask = subtask
            print(message)

    def handle_input(self, character):
        # only handle input if strategy has been launched
        # Will we assume that each key command corresponds to a certain task?
        # i.e. 5 = kick_off(kicking), 6 = kick_off(goalie), 7 = kick_off(other)
        if self.launched:
            if character == 'q':
                print("[STRATEGY] quitting...")
                self.close()

            elif character == ' ':
                print("[STRATEGY] halting")
                self.task = "idle"
                self.planner.subtask = "idle"
                self.planner.halt()

            elif character == '#':
                old_defending_side = self.world_state.defending_side
                self.world_state.swap_goals()
                new_defending_side = self.world_state.defending_side
                print("[STRATEGY] CONFIRMATION - changed defending side from '{}' to '{}'"
                      .format(old_defending_side, new_defending_side))

            elif character == 'p':
                self.task = "something"
                self.subtask = "intercept pass"
                print("[STRATEGY] intercepting pass")

            elif character == 's':
                self.task = "something"
                self.subtask = "intercept shot"
                print("[STRATEGY] intercepting shot")

            elif character == 'r':
                self.task = "move to receive pass"
                self.subtask = "move to receive pass"
                print("[STRATEGY] move to receive pass")

            elif character == '1':
                self.task = "something"
                self.subtask = "penalty shoot"
                print("[STRATEGY] penalty shooting")

            elif character == '2':
                self.task = "something"
                self.subtask = "panic shoot"
                print("[STRATEGY] panic shooting")

            elif character == 'k':
                self.task = "something"
                self.subtask = "kick off"
                print("[STRATEGY] kicking off")

            elif character == 'b':
                self.task = "acquire ball"
                self.subtask = "acquire ball"
                print("[STRATEGY] acquiring the ball")

    def handle_message_group09(self, message):
        self.world_state.handle_message('group09', message)
        self.planner.handle_message(message)

    def handle_message_group10(self, message):
        pass
