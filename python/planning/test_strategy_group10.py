class TestStrategyGroup10:
    def __init__(self, world_state, planner):
        self.quit = False
        self.launched = False
        self.world_state = world_state
        self.planner = planner

        self.task = "idle"
        self.subtask = None

    def close(self):
        self.quit = True

    def launch(self):
        print("[STRATEGY GROUP 10] launched")
        self.launched = True
        self.world_state.set_centre()
        self.world_state.set_goals()
        self.world_state.set_zones()
        self.world_state.set_receive_zones()
        self.world_state.update()

        while not self.quit:
            if self.task == "idle":
                continue

            self.world_state.update()
            self.determine_subtask()
            self.planner.execution_step(self.subtask)
        self.launched = False

    def determine_subtask(self):
        if self.task == "acquire ball":
            # TODO: This is BREAKING the subtask: idle is being
            # TODO: set before the grabbers can open
            # if self.world_state.who_has_ball() is None:
            #     self.subtask = "acquire ball"
            #
            # elif self.world_state.who_has_ball().name == "blue + pink" and self.world_state.who_has_ball() is not None:
            #     print("[STRATEGY] ball acquired")
            #     self.task = "idle"
            # else:
            self.subtask = "acquire ball"
        if self.task == "pass":
            if not self.planner.subtask_complete("turn to ally"):
                self.set_subtask("turn to ally", "[DEFENDER] turning to ally to pass")
            else:
                self.subtask = "pass"

    def set_subtask(self, subtask, message):
        if self.subtask != subtask:
            self.subtask = subtask
            print(message)

    def handle_input(self, character):
        # only handle input if strategy has been launched
        if self.launched:
            if character == 'q':
                print("[STRATEGY] quitting...")
                self.close()
            elif character == ' ':
                print("[STRATEGY] halting")
                self.task = "idle"
                self.planner.force_halt()

            elif character == 'b':
                print("[STRATEGY] acquiring ball")
                self.task = "acquire ball"
            elif character == 'k':
                print("[STRATEGY] panic shoot")
                self.task = 'qwe'
                self.subtask = "panic shoot"
            elif character == 'p':
                print("[STRATEGY] passing")
                self.task = 'qwe'
                self.subtask = "pass"
            elif character == 'o':
                print("[STRATEGY] turn to ally")
                self.task = "quweiq"
                self.subtask = 'turn to ally'
            elif character == 'x':
                print("[STRATEGY] defend goal from ball")
                self.task = "quweiq"
                self.subtask = "defend goal from ball"
            elif character == 'f':
                print("[STRATEGY] defend")
                self.task = "quweiq"
                self.subtask = "defend penalty"

            elif character == 'z':
                print("[STRATEGY] shoot")
                self.task = "quweiq"
                self.subtask = "shoot"
            elif character == '/':
                print("[STRATEGY] receive pass")
                self.task = "quweiq"
                self.subtask = "receive pass"

    def handle_message_group09(self, message):
        pass

    def handle_message_group10(self, message):
        self.world_state.handle_message('group10', message)
