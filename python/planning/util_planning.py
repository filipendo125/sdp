import math
import operator
from random import random, uniform


class Util:
    def __init__(self):
        pass

    @staticmethod
    def get_direction(dept, dest):
        """
        angles: right = 0 (origin), down = -90, top = 90, left = 180
        range: -180 < angle <= 180
        """

        # y axis is reflected
        x = dest[0] - dept[0]
        y = dest[1] - dept[1]
        # tan(angle) = opposite / adjacent
        # using atan2 look at the api for it in math
        distance = math.sqrt(x * x + y * y)
        angle = math.degrees(math.atan2(y, x))

        return {'distance': distance, 'angle': angle}

    @staticmethod
    def get_turn_angle(facing, angle):
        # angles: right = 0 (origin), down = -90, top = 90, left = 180
        # range: -180 < angle <= 180
        turn_angle = (facing - angle) % 360
        if turn_angle > 180:
            turn_angle -= 360
        return -turn_angle

    @staticmethod
    def get_smallest_angle(angle1, angle2):
        return abs(Util.get_turn_angle(angle1, angle2))

    @staticmethod
    def get_point_in_line(pos1, pos2, percentage):
        return ((pos2[0] - pos1[0]) * percentage) + pos1[0], ((pos2[1] - pos1[1]) * percentage) + pos1[1]

    @staticmethod
    def get_opposite_heading(heading):
        heading = int(round(heading))
        if -179 <= heading <= 0:
            heading += 180
        elif 1 <= heading <= 180:
            heading -= 180
        assert heading in range(-179, 181)
        return heading

    @staticmethod
    def intercept_goal(robot):
        goal_coords = robot.attacking_goal_line
        if ((robot.heading - 90) % 180) == 0:
            return None
        x = robot.attacking_goal_line[0][0]
        y = robot.position[1] + (x - robot.position[0]) * math.tan(math.radians(robot.heading))
        if goal_coords[0][1] < y < goal_coords[1][1]:
            return x, y
        else:
            return None

    @staticmethod
    def get_intercept_point(position, intercept_line):
        """
        :param intercept_line: a list of two points, one for each enemy, representing the line between the enemies
        :type intercept_line: list[tuple(float,float)]
        """
        # calculates the slope of a line between 2 enemy robots
        a = (intercept_line[1][1] - intercept_line[0][1])/(intercept_line[1][0] - intercept_line[0][0])
        # calculates constant 'c' in the Standard Line Equation Form
        c = intercept_line[0][1] - a*intercept_line[0][0]
        b = -1

        denominator = a*a+b*b

        x_numerator = b*(b*position[0] - a*position[1]) - a*c
        y_numerator = a*(-b*position[0] + a*position[1]) - b*c
        return [x_numerator/denominator, y_numerator/denominator]

    @staticmethod
    def line_intersection(line1, line2):
        xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
        ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

        def det(a, b):
            return a[0] * b[1] - a[1] * b[0]

        div = det(xdiff, ydiff)
        if div == 0:
            raise Exception('lines do not intersect')

        d = (det(*line1), det(*line2))
        x = det(d, xdiff) / div
        y = det(d, ydiff) / div
        return x, y

    @staticmethod
    def do_they_intersect((A, B), (C, D)):  # A,B,C,D are all tuples. AB is one line, CD is the other
        # http://bryceboe.com/2006/10/23/line-segment-intersection-algorithm/
        def ccw(A, B, C):
            return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])
        return ccw(A, C, D) != ccw(B, C, D) and ccw(A, B, C) != ccw(A, B, D)

    @staticmethod
    def get_midpoint(goal_coordinates):
        return tuple([int((item1 + item2)/2) for item1, item2 in zip(goal_coordinates[0], goal_coordinates[1])])

    @staticmethod
    def get_intercept_point_or_midpoint(position, intercept_line):
        x, y = Util.get_intercept_point(position, intercept_line)
        if (not (intercept_line[0][0] < x < intercept_line[1][0] or intercept_line[1][0] < x < intercept_line[0][0])) or \
                (not (intercept_line[0][1] < y < intercept_line[1][1] or intercept_line[1][1] < y < intercept_line[0][1])) or \
                x < 30 or y < 30 or x > 604 or y > 400:
            x, y = Util.get_midpoint(intercept_line)
        return x, y

    @staticmethod
    def can_score(goal_cords, robot, list_of_robots):
        if Util.intercept_goal(robot) is None:
            return False
        else:
            list_of_obstacles = Util.get_blocking_robots(robot, list_of_robots)
            return len(list_of_obstacles) == 0

    @staticmethod
    def get_blocking_robots(robot, list_of_robots, heading=None):
        if heading is None:
            heading = robot.heading
        heading = float(heading)
        slope = math.tan(math.radians(heading))
        b = robot.position[1]-slope*robot.position[0]
        list_of_obstacles = [robots for robots in list_of_robots if robot.name != robots.name and
                             abs(robots.position[1] - slope*robots.position[0]-b) <= 30]
        return list_of_obstacles

    @staticmethod
    def point_in_poly(x, y, poly):
    # http://stackoverflow.com/questions/16625507/python-checking-if-point-is-inside-a-polygon
    # http://www.ariel.com.au/a/python-point-int-poly.html
        n = len(poly)
        inside = False
        p1x, p1y = poly[0]
        for i in range(n+1):
            p2x, p2y = poly[i % n]
            if y > min(p1y, p2y):
                if y <= max(p1y, p2y):
                    if x <= max(p1x, p2x):
                        if p1y != p2y:
                            xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                        if p1x == p2x or x <= xints:
                            inside = not inside
            p1x, p1y = p2x, p2y
        return inside

    @staticmethod
    def point_in_defense_zone(zones, point, zone):
        left_zone, right_zone = zones
        if zone == "left":
            return Util.point_in_poly(point[0], point[1], left_zone)
        else:
            return Util.point_in_poly(point[0], point[1], right_zone)

    @staticmethod
    def set_receive_point(receiving_zones, attacking_side, center):
        # x-values
        # top_of_top_receive_zone
        # bottom_of_top_receive_zone
        # top_of_bottom_receive_zone
        # bottom_of_bottom_receive_zone
        # y-values
        # left_zone_right_side
        # right_zone_left_side
        top_of_top_receive_zone, bottom_of_top_receive_zone, top_of_bottom_receive_zone, \
            bottom_of_bottom_receive_zone, left_zone_right_side, right_zone_left_side = receiving_zones

        if attacking_side == "right":
            if random() > .5:
                x = round(uniform(center[0], right_zone_left_side))
                y = round(uniform(top_of_top_receive_zone, bottom_of_top_receive_zone))
            else:
                x = round(uniform(center[0], right_zone_left_side))
                y = round(uniform(top_of_bottom_receive_zone, bottom_of_bottom_receive_zone))
        else:
            if random() > .5:
                x = round(uniform(left_zone_right_side, center[0]))
                y = round(uniform(top_of_top_receive_zone, top_of_bottom_receive_zone))
            else:
                x = round(uniform(left_zone_right_side, center[0]))
                y = round(uniform(top_of_bottom_receive_zone, bottom_of_bottom_receive_zone))

        return x, y

