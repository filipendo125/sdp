from util_planning import Util

class BaseRobot:
    def __init__(self, name, role):
        self.role = role
        self.name = name
        self.heading = None
        self.position = None
        self.ball_in_range = None
        self.ball_in_left = None
        self.ball_in_right = None
        self.ball_in_bottom = None
        self.has_ball_vision = None
        self.visible = False

        self.ally = None
        self.enemy1 = None
        self.enemy2 = None

        self.centre = (240, 240)
        self.defending_goal = None
        self.attacking_goal = None
        self.defending_goal_line = None
        self.attacking_goal_line = None
        self.zones = None
        self.receive_zones = None

        self.direction = {"ball": None, "ally": None, "attacking goal": None, "score": None}
        self.facing_attacking_goal = None

    def set_ally(self, ally):
        self.ally = ally

    def set_centre(self, centre):
        self.centre = centre

    def set_goals(self, defending_goal, attacking_goal, goal_coords):
        self.defending_goal = defending_goal
        self.attacking_goal = attacking_goal
        if self.defending_goal == 'left':
            self.defending_goal_line = goal_coords[0:2]
            self.attacking_goal_line = goal_coords[2:4]
        else:
            self.defending_goal_line = goal_coords[2:4]
            self.attacking_goal_line = goal_coords[0:2]

    # REVISITED
    def set_zones(self, zones):
        self.zones = zones

    # REVISITED
    def set_receive_zones(self, receive_zones):
        self.receive_zones = receive_zones

    def update(self, vision_robot):
        assert vision_robot.name == self.name
        self.visible = vision_robot.visible
        if self.visible:
            self.heading = vision_robot.heading
            self.position = vision_robot.position
            self.ball_in_range = vision_robot.ball_in_range
            self.ball_in_left = vision_robot.ball_in_left
            self.ball_in_right = vision_robot.ball_in_right
            self.ball_in_bottom = vision_robot.ball_in_bottom
            self.has_ball_vision = vision_robot.has_ball

    def calculate_properties(self, *args):
        if self.visible:
            ball_position = args[0]

            self.direction["ball"] = Util.get_direction(self.position, ball_position)
            if self.ally.visible:
                self.direction["ally"] = Util.get_direction(self.position, self.ally.position)
            self.facing_attacking_goal = Util.intercept_goal(self) is not None

            self.direction['attacking goal'] = Util.get_direction(self.position,
                                                                  Util.get_midpoint(self.attacking_goal_line))


class EnemyRobot(BaseRobot):
    def __init__(self, name, role):
        BaseRobot.__init__(self, name, role)
