from time import time
import traceback


class Strategy:
    def __init__(self, world_state, planner_group09, planner_group10):
        self.quit = False
        self.launched = False
        self.world_state = world_state
        self.planners = {'group09': planner_group09, 'group10': planner_group10}

        self.input = None
        self.attacker = None
        self.task = "idle"
        self.subtasks = {'group09': "idle", 'group10': "idle"}
        self.flags = {"ball position": None}
        self.timestamps = {"receive pass": None, "kick off": None}
        self.kick_off_started = False

    @property
    def defender(self):
        if self.attacker == 'group09':
            return 'group10'
        else:
            return 'group09'

    def close(self):
        self.quit = True

    def launch(self):
        print("[STRATEGY BOTH] launched")
        self.launched = True
        try:
            self.world_state.set_centre()
            self.world_state.set_goals()
            self.world_state.set_zones()
            self.world_state.set_receive_zones()
            self.world_state.update()
            self.planners['group09'].has_ball = False
        except Exception as e:
            print("[STRATEGY] RESTART REQUIRED - exception initialising strategy")
            raise e

        while not self.quit:
            if self.task == "idle":
                continue

            try:
                self.world_state.update()
            except:
                print("[STRATEGY] exception in world_state.update(): ")
                traceback.print_exc()

            try:
                self.determine_subtasks()
            except:
                print("[STRATEGY] exception in determine_subtasks(): ")
                traceback.print_exc()

            try:
                self.planners['group09'].execution_step(self.subtasks['group09'])
            except:
                print("[STRATEGY] exception in group 9 planner with subtask {}: ".format(self.subtasks['group09']))
                traceback.print_exc()

            try:
                self.planners['group10'].execution_step(self.subtasks['group10'])
            except:
                print("[STRATEGY] exception in group 10 planner with subtask {}: ".format(self.subtasks['group10']))
                traceback.print_exc()

        self.launched = False

    def determine_subtasks(self):
        timestamp = time()

        # if self.world_state.who_has_ball() is None:
        #     print(self.world_state.who_has_ball())
        # else:
        #     print(self.world_state.who_has_ball().role)

        if self.world_state.robots['group09'].ball_in_range:
            print("True")

        # penalties
        if self.task == "start penalty attack":
            if self.subtasks[self.attacker] == "start" and self.subtasks[self.defender] == "start":
                self.subtasks[self.attacker] = "acquire ball"
                print("[ATTACKER] acquiring ball")
            elif self.subtasks[self.attacker] == "acquire ball" and self.subtask_complete(self.attacker):
                self.subtasks[self.attacker] = "penalty shoot"
                print("[ATTACKER] shooting")
            elif self.subtasks[self.attacker] == "penalty shoot" and self.subtask_complete(self.attacker):
                print("[STRATEGY] penalty shot taken, playing ball")
                self.task = "play ball"

        elif self.task == "start penalty defend":
            if self.subtasks[self.attacker] == "start" and self.subtasks[self.defender] == "start":
                self.subtasks[self.defender] = "defend penalty"
                print("[STRATEGY] waiting for kick before playing ball")
                self.flags["ball position"] = self.world_state.ball_position
                self.task = "play ball when ball moves"

        elif self.kick_off_started and timestamp - self.timestamps["kick off"] > 8:
            print("[STRATEGY] kick off time out, playing ball")
            self.task = "play ball"
            self.kick_off_started = False

        # kick offs
        elif self.task == "start kick off attack":
            if self.subtasks[self.attacker] == "start" and self.subtasks[self.defender] == "start":
                self.subtasks[self.attacker] = "acquire ball"
                print("[ATTACKER] acquiring ball")
                self.timestamps["kick off"] = timestamp
                self.kick_off_started = True
            elif self.subtasks[self.attacker] == "acquire ball" and self.subtask_complete(self.attacker):
                self.subtasks[self.attacker] = "kick off"
                print("[ATTACKER] kicking off")
            elif self.subtasks[self.attacker] == "kick off" and self.subtask_complete(self.attacker):
                print("[STRATEGY] kick off taken, playing ball")
                self.task = "play ball"

        elif self.task == "start kick off defend":
            if self.subtasks[self.attacker] == "start" and self.subtasks[self.defender] == "start":
                print("[STRATEGY] waiting for kick before playing ball")
                self.flags["ball position"] = self.world_state.ball_position
                self.task = "play ball when ball moves"

        elif self.task == "play ball when ball moves" and self.world_state.has_ball_moved(self.flags["ball position"]):
            print("[STRATEGY] ball moved, playing ball")
            self.task = "play ball"

        # playing ball
        elif self.task == "play ball":
            # Attacker Strategy

            # only set a subtask if we are not waiting to receive a pass
            # if self.subtasks[self.attacker] != "receive pass" \
            #         or timestamp - self.timestamps["receive pass"] > 5 or \
            #         self.subtask_complete(self.attacker, "receive pass"):

            if self.who_has_ball() is None:
                # if the defender is closer and the ball is on our side, let the defender get it
                if self.world_state.ball_side() == "ours" and \
                        self.world_state.closest_robot_to_ball() == self.defender:
                    self.set_subtask(self.attacker, "move to receive pass",
                                     "[ATTACKER] pre-emptively moving to receive pass")
                else:
                    self.set_subtask(self.attacker, "acquire ball", "[ATTACKER] acquiring ball")

            elif self.who_has_ball().role == self.attacker:
                # if self.world_state.ball_side() == "ours":
                    self.set_subtask(self.attacker, "shoot", "[ATTACKER] shooting")
                # else:
                    # if the attacker has the ball on our side, the attacker becomes the defender
                    # self.swap_roles()

            elif self.who_has_ball().role == 'enemy1' or self.who_has_ball().role == 'enemy2':
                if self.world_state.heading_closer_to_attacking_goal_or_ally(self.who_has_ball()) \
                        == "goal":
                    self.set_subtask(self.attacker, "intercept shot", "[ATTACKER] intercepting shot")
                else:
                    self.set_subtask(self.attacker, "intercept pass", "[ATTACKER] intercepting pass")

            else:  # self.who_has_ball() == self.defender:
                # if not self.subtask_complete(self.attacker, "move to receive pass"):
                self.set_subtask(self.attacker, "move to receive pass",
                                 "[ATTACKER] moving to receive a pass")
                # elif self.subtasks[self.attacker] != "receive pass":
                #     self.subtasks[self.attacker] = "receive pass"
                #     print("[ATTACKER] awaiting pass")
                #     self.timestamps["receive pass"] = timestamp

                    # Basically everything happens in here
            # see http://hmi.ewi.utwente.nl/robotsoccernieuw/documents/thesis_roelof_heddema.pdf for inspiration
            # pages 23~30 for where I'm coming from

            # Defender Strategy
            # Don't leave our defense zone unless subtask is "acquire ball"
            # If the ball is on the other side of the pitch, the defense enters a passive state.
            # In the passive state, the robot tries to maximize goal coverage (by moving to a particular
            # position and orientation depending on the ball's y coordinates)
            #
            # If the ball is on our side of the pitch, the defender enters an active state.
            #
            # There are several situations in this case.
            #
            # 1. If no one has the ball, it's not moving to our goal, and we're the closest robot,
            # we want to get the ball and clear it away from the goal - by passing or panic shooting
            #
            # 2. If the enemy robot has the ball we try to orientate ourselves so that we can successfully block
            # (and maybe catch) the attempted shot.
            #
            # 3. Else we just try and maximize goal coverage by positioning ourselves between the ball and the goal
            # using "defend goal from ball"
            # if self.who_has_ball() is not None:
            #     print("[STRATEGY] '{}' has the ball".format(self.who_has_ball().role))

            if self.world_state.ball_side() == "theirs":
                self.set_subtask(self.defender, "align with ball", "[DEFENDER] aligning with ball")

            else:
                if self.who_has_ball() is None:
                    if self.world_state.closest_robot_to_ball() == self.defender:  # \
                            # and not self.world_state.ball_moving_to_defending_goal():
                        self.set_subtask(self.defender, "acquire ball", "[DEFENDER] acquiring ball")
                    else:
                        self.set_subtask(self.defender, "defend goal from ball",
                                         "[DEFENDER] defending the goal from the ball")

                elif self.who_has_ball().role == self.defender:
                    # if timestamp - self.planners[self.defender].timestamps["acquired ball"] < 10:
                        if not self.subtask_complete(self.defender, "turn to ally"):
                            self.set_subtask(self.defender, "turn to ally", "[DEFENDER] turning to ally to pass")
                        else:  # if self.subtasks[self.attacker] == "receive pass":
                            self.set_subtask(self.defender, "pass", "[DEFENDER] passing the ball")
                    # else:
                    #     self.set_subtask(self.defender, "panic shoot", "[DEFENDER] could not pass in time - panicking")

                elif self.who_has_ball().role == 'enemy1' or self.who_has_ball() == 'enemy2':
                    self.set_subtask(self.defender, "defend enemy shot", "[DEFENDER] defending the enemy's shot")

                else:  # self.who_has_ball() == self.attacker:
                    self.set_subtask(self.defender, "defend goal from ball",
                                     "[DEFENDER] defending the goal from the ball")

    def who_has_ball(self):
        return self.world_state.who_has_ball()

    def set_subtask(self, robot, subtask, message):
        if self.subtasks[robot] != subtask:
            self.subtasks[robot] = subtask
            print(message)

    def subtask_complete(self, robot, subtask=None):
        assert robot in ['group09', 'group10']
        if subtask is None:
            subtask = self.subtasks[robot]

        if subtask == "acquire ball" or subtask == "receive pass":
            return self.planners[robot].subtask_complete("has ball")

        elif subtask == "turn to ally":
            return self.planners[robot].subtask_complete("turn to ally")

        elif subtask == "shoot" or "kick off" or "panic shoot" or "penalty shoot":
            return self.planners[robot].subtask_complete("kicked")

        elif subtask == "move to receive pass":
            return self.planners[robot].subtask_complete("move to receive pass")

    def swap_roles(self):
        self.attacker = self.defender
        print("[STRATEGY] ROLES SWAPPED - ATTACKER: '{}', DEFENDER: '{}'".format(self.attacker, self.defender))

    def handle_input(self, character):
        # only handle input if strategy has been launched
        # Will we assume that each key command corresponds to a certain task?
        # i.e. 5 = kick_off(kicking), 6 = kick_off(goalie), 7 = kick_off(other)
        if self.launched:
            ####################

            if character == 'q':
                print("[STRATEGY] quitting...")
                self.close()

            elif character == ' ':
                print("[STRATEGY] halting")
                self.task = "idle"
                self.planners['group09'].halt()
                self.planners['group10'].halt()
                self.planners['group09'].subtask = "idle"
                self.planners['group10'].subtask = "idle"

            elif character == '#':
                old_defending_side = self.world_state.defending_side
                self.world_state.swap_goals()
                new_defending_side = self.world_state.defending_side
                print("[STRATEGY] CONFIRMATION - changed defending side from '{}' to '{}'"
                      .format(old_defending_side, new_defending_side))

            elif character == 'c':
                print("[STRATEGY] INPUT REQUIRED - restarting communications for which robot (9 or 0)?")
                self.input = "restart communications"

            elif self.input == "restart communications" and (character == '9' or character == '0'):
                if character == '9':
                    print("[STRATEGY] CONFIRMATION - restarting group 9 communications")
                    self.planners['group09'].com.restart()
                elif character == '0':
                    print("[STRATEGY] CONFIRMATION - restarting group 10 communications")
                    self.planners['group10'].com.restart()

            elif character == '=':
                print("[ATTACKER] {} ({})".format(self.subtasks[self.attacker], self.attacker))
                print("[DEFENDER] {} ({})".format(self.subtasks[self.defender], self.defender))

            elif character == '/':
                self.swap_roles()
                print("ATTACKER {}, DEFENDER {}".format(self.attacker, self.defender))

            ####################

            # If there is a penalty call, press p
            elif character == 'p':
                print("[STRATEGY] INPUT REQUIRED - penalty: press a for attack, d for defend")
                self.input = "penalty"

            # If we are at kick off, press k
            elif character == 'k':
                print("[STRATEGY] INPUT REQUIRED - kick off: press l for defending left side, "
                      "r for defending right side")
                self.input = "kick off side"

            # If we are just playing ball, press b
            elif character == 'b':
                print("[STRATEGY] playing ball")
                self.task = "play ball"
                if self.attacker is None:
                    self.attacker = 'group09'

            # Decide whether we are kicking or receiving the penalty
            elif self.input == "penalty":
                if character == 'a':
                    self.input = "penalty attack"
                    print("[STRATEGY] INPUT REQUIRED - penalty attack: which robot (9 or 0) is shooting?")
                elif character == 'd':
                    self.input = "penalty defend"
                    print("[STRATEGY] INPUT REQUIRED - penalty defend: which robot (9 or 0) is goalie?")

            # Set the sides during kick off (it could have changed)
            elif self.input == "kick off side" and (character == 'l' or character == 'r'):
                if character == 'l':
                    self.world_state.set_goals("left")
                    print("[STRATEGY] CONFIRMATION - defending the left side")
                elif character == 'r':
                    self.world_state.set_goals("right")
                    print("[STRATEGY] CONFIRMATION - defending the right side")
                self.input = "kick off"
                print("[STRATEGY] INPUT REQUIRED - kick off: press a for attack, d for defend")

            # Decide whether we are kicking or receiving the kick off
            elif self.input == "kick off":
                if character == 'a':
                    self.input = "kick off attack"
                    print("[STRATEGY] INPUT REQUIRED - kick off attack: which robot (9 or 0) is taking it?")
                elif character == 'd':
                    self.input = "kick off defend"
                    print("[STRATEGY] INPUT REQUIRED - kick off defend: which robot (9 or 0) is goalie?")

            # Decide which robot will shoot the penalty
            elif self.input == "penalty attack" and (character == '9' or character == '0'):
                if character == '9':
                    self.attacker = 'group09'
                    print("[STRATEGY] CONFIRMATION - group 9 shooting penalty")
                else:
                    self.attacker = 'group10'
                    print("[STRATEGY] CONFIRMATION - group 10 shooting penalty")
                self.task = "start penalty attack"
                self.subtasks['group09'] = "start"
                self.subtasks['group10'] = "start"

            # Decide which robot will defend the shot
            elif self.input == "penalty defend" and (character == '9' or character == '0'):
                if character == '9':
                    self.attacker = 'group10'
                    print("[STRATEGY] CONFIRMATION - group 9 goalie for penalty")
                elif character == '0':
                    self.attacker = 'group09'
                    print("[STRATEGY] CONFIRMATION - group 10 goalie for penalty")
                self.task = "start penalty defend"
                self.subtasks['group09'] = "start"
                self.subtasks['group10'] = "start"

            # Decide which robot will start the kick off
            elif self.input == "kick off attack":
                if character == '9':
                    self.attacker = 'group09'
                    print("[STRATEGY] CONFIRMATION - group 9 initiating kick off")
                elif character == '0':
                    self.attacker = 'group10'
                    print("[STRATEGY] CONFIRMATION - group 10 initiating kick off")
                self.task = "start kick off attack"
                self.subtasks['group09'] = "start"
                self.subtasks['group10'] = "start"

            # Assign goalie
            elif self.input == "kick off defend":
                if character == '9':
                    self.attacker = 'group10'
                    print("[STRATEGY] CONFIRMATION - group 9 goalie after kick off")
                elif character == '0':
                    self.attacker = 'group09'
                    print("[STRATEGY] CONFIRMATION - group 10 goalie after kick off")
                self.task = "start kick off defend"
                self.subtasks['group09'] = "start"
                self.subtasks['group10'] = "start"

            # Tiny testing suite for individual subtasks
            elif self.input == "custom":
                if character == '1':
                    self.subtasks['group10'] = "acquire ball"
                elif character == '2':
                    self.subtasks['group10'] = "shoot"
                elif character == '3':
                    self.subtasks['group10'] = "defend penalty"
                elif character == '4':
                    self.subtasks['group10'] = "kick off"
                elif character == '5':
                    self.subtasks['group10'] = "align with ball"
                elif character == '6':
                    self.subtasks['group10'] = "defend goal from ball"
                elif character == '7':
                    self.subtasks['group10'] = "turn to ally"
                elif character == '8':
                    self.subtasks['group10'] = "pass"
                elif character == '9':
                    self.subtasks['group10'] = "panic shoot"
                elif character == '0':
                    self.subtasks['group10'] = "defend enemy shot"
                elif character == '-':
                    self.subtasks['group10'] = "move to receive pass"
                elif character == '=':
                    self.subtasks['group10'] = "intercept shot"
                elif character == 'w':
                    self.subtasks['group10'] = "intercept pass"
                elif character == 'e':
                    self.subtasks['group10'] = "receive pass"
                else:
                    print "Not something that I know how to do:", character

    def handle_message_group09(self, message):
        self.world_state.handle_message('group09', message)
        self.planners['group09'].handle_message(message)

    def handle_message_group10(self, message):
        self.world_state.handle_message('group10', message)
