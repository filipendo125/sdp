#!/usr/bin/env python

import sys

# add parent directory to path
import os.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from communication import Communication

def main(level):
    with Communication() as com:
        com.grab_and_kick(level)

if __name__ == '__main__':
    main(level=int(sys.argv[1]))
