#!/usr/bin/env python

import sys
from time import sleep

# add parent directory to path
import os.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from communication import Communication

def main(binary_file_path, frequency):
    """
    :param frequency: In Hz, for example, every 20ms = 50Hz, every 1000ms = 1Hz.
    """
    with Communication() as com:
        with open(binary_file_path, "rb") as f:
            byte = f.read(1)
            while byte != "":
                com.send_byte(byte)
                sleep(1/frequency)
                byte = f.read(1)

if __name__ == '__main__':
    main(binary_file_path=sys.argv[1], frequency=float(sys.argv[2]))
