#!/usr/bin/env python

import sys

# add parent directory to path
import os.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from communication import Communication

def main(x, y, angle):
    with Communication() as com:
        if x > 0:
            com.move_forward(abs(x))
        elif x < 0:
            com.move_backward(abs(x))

        current_angle = 0
        if y > 0:
            com.turn_clockwise(90)
            current_angle = 90
            com.move_forward(abs(y))
        elif y < 0:
            com.turn_anticlockwise(90)
            current_angle = 270
            com.move_forward(abs(y))

        target_angle = angle % 360
        turn_angle = smallest_signed_angle_between(current_angle, target_angle)
        if turn_angle > 0:
            com.turn_clockwise(abs(turn_angle))
        elif turn_angle < 0:
            com.turn_anticlockwise(abs(turn_angle))

def smallest_signed_angle_between(x, y):
    a = (x - y) % 360
    b = (y - x) % 360
    return -a if a < b else b

if __name__ == '__main__':
    main(x=int(sys.argv[1]), y=int(sys.argv[2]), angle=int(sys.argv[3]))
