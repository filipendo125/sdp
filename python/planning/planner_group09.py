from time import time
from random import choice, uniform
from util_planning import Util
from base_robot import BaseRobot


class PlannerGroup09:
    def __init__(self, world_state, com):
        self.world_state = world_state
        self.com = com
        self.robot = self.world_state.robots['group09']

        self.action = None
        self.subtask = None
        self.subtask_started = False
        self.timestamps = {"acquired ball": None, "ungrabbed": None, "grab force": time(), "kicked": time(),
                           "move": None, "turn": None}

        self.moving = False
        self.direction = None
        self.avoiding_robot = False
        self.collision = None
        self.bearing = None
        self.go_to_desired_position = False
        self.receive_point = None

        self.start_position = None
        self.start_heading = None

        self.has_ball = False

        self.grabbed = True
        self.kicked = False

    def handle_message(self, message):
        print("[COMMS GROUP 9] message received: {}".format(message))
        if message == "*":
            self.moving = False
        elif message == "B":
            self.timestamps["acquired ball"] = time()
            self.grabbed = True
        elif message == "F":
            self.collision = "forward"
        elif message == "L":
            self.collision = "left"
        elif message == "R":
            self.collision = "right"

    def execution_step(self, subtask):
        self.subtask_started = self.subtask != subtask
        self.subtask = subtask

        try:
            self.has_ball = self.world_state.who_has_ball().role == 'group09'
        except AttributeError:
            self.has_ball = False

        timestamp = time()
        heading = self.robot.heading
        position = self.robot.position
        defending_goal_midpoint = Util.get_midpoint(self.robot.defending_goal_line)
        self.go_to_desired_position = False

        if timestamp - self.timestamps["kicked"] >= 1:
            self.kicked = False

        if not self.grabbed and timestamp - self.timestamps["ungrabbed"] >= 5:
            self.grab_force()

        try:
            naughty = Util.point_in_defense_zone(self.robot.zones, self.robot.position, self.robot.attacking_goal) or \
                (Util.point_in_defense_zone(self.robot.zones, self.robot.position, self.robot.defending_goal) and
                 Util.point_in_defense_zone(self.robot.zones, self.robot.ally.position, self.robot.defending_goal))
        except:
            naughty = False

        if naughty:
            self.robot.set_desired_position(self.robot.centre)
            self.go_to_desired_position = True

        # always grab the ball if possible, no matter what the subtask is
        elif self.robot.ball_in_range and not self.has_ball and not self.grabbed:
            self.grab()
            self.halt()

        # don't bash into walls
        elif self.collision is not None:
            if self.collision == "forward":
                self.moving = True
                self.com.move_backward(50)  # TODO - calibrate
            elif self.collision == "left":
                self.moving = True
                self.com.turn_clockwise(50)  # TODO - calibrate
            elif self.collision == "right":
                self.moving = True
                self.com.turn_anticlockwise(50)  # TODO - calibrate
            self.collision = None

        # turn towards the ball, then ungrab and move towards it, grabbing and halting when it is within range
        elif self.subtask == "acquire ball":
            try:
                naughty = Util.point_in_defense_zone(self.robot.zones, self.world_state.ball_position, self.robot.attacking_goal) or \
                    (Util.point_in_defense_zone(self.robot.zones, self.world_state.ball_position, self.robot.defending_goal) and
                        Util.point_in_defense_zone(self.robot.zones, self.robot.ally.position, self.robot.defending_goal))
            except:
                naughty = False

            if naughty:
                self.robot.set_desired_position(self.robot.centre)
                self.go_to_desired_position = True
                # print("[PLANNER GROUP 9] ball in a naughty place, retreating to centre")
            else:
                angle_to_ball = Util.get_turn_angle(heading, self.robot.direction["ball"]['angle'])
                if self.robot.ball_in_left or self.robot.ball_in_right:
                    self.grab()
                    self.move(-50, position)  # TODO - calibrate
                elif self.robot.ball_in_bottom:
                    self.grab()
                    self.move(50, position)  # TODO - calibrate
                elif not self.robot.within_tolerance_grab_angle(angle_to_ball):
                    self.grab()
                    self.turn(angle_to_ball, heading)
                else:
                    distance_to_ball = self.robot.direction["ball"]['distance']
                    if self.grabbed and self.robot.ball_in_range:
                        self.move(-50, position)  # TODO - calibrate
                    elif not self.robot.ball_in_range:
                        self.ungrab()
                        self.move(distance_to_ball, position)

        elif self.subtask == "penalty shoot":
            if self.has_ball:
                if self.subtask_started or self.bearing is None:
                    if self.robot.defending_goal == "left":
                        self.bearing = round(uniform(5, 15))
                        if choice(["left", "right"]) == "left":
                            self.bearing = -self.bearing
                    else:
                        self.bearing = round(uniform(175, 165))
                        if choice(["left", "right"]) == "left":
                            self.bearing = -self.bearing

                angle_to_bearing = Util.get_turn_angle(heading, self.bearing)
                if not self.robot.within_tolerance_angle(angle_to_bearing):
                    self.turn(angle_to_bearing, heading)
                else:
                    self.kick_power(100)

        elif self.subtask == "defend penalty":
            # our robot cannot implement this subtask
            pass

        elif self.subtask == "kick off":
            if self.has_ball:
                if self.subtask_started or self.bearing is None:
                    if self.robot.defending_goal == "left":
                        self.bearing = round(uniform(23, 67))
                        if choice(["left", "right"]) == "left":
                            self.bearing = -self.bearing
                    else:
                        self.bearing = round(uniform(113, 157))
                        if choice(["left", "right"]) == "left":
                            self.bearing = -self.bearing

                angle_to_bearing = Util.get_turn_angle(heading, self.bearing)
                if not self.robot.within_tolerance_angle(angle_to_bearing):
                    self.turn(angle_to_bearing, heading)
                else:
                    self.kick_power(100)

        # turn our robot to face our ally
        elif self.subtask == "turn to ally":
            angle_to_ally = Util.get_turn_angle(heading, self.robot.direction["ally"]['angle'])
            if not self.robot.within_tolerance_angle(angle_to_ally):
                self.turn(angle_to_ally, heading)

        elif self.subtask == "pass" and self.has_ball:
            self.kick_distance(self.robot.direction["ally"]['distance'])

        elif self.subtask == "panic shoot" or self.subtask == "shoot":
            if self.has_ball:
                if self.robot.facing_attacking_goal:
                    self.kick_power(100)
                else:
                    angle_to_goal = Util.get_turn_angle(heading, self.robot.direction["attacking goal"]['angle'])
                    self.turn(angle_to_goal, heading)

        elif self.subtask == "move to receive pass" or self.subtask == "intercept shot" or \
                self.subtask == "intercept pass" or self.subtask == "align with ball" or \
                self.subtask == "defend goal from ball" or self.subtask == "defend enemy shot":

            if self.subtask == "move to receive pass":
                # if self.subtask_started:
                #     self.receive_point = Util.set_receive_point(self.robot.zones, self.robot.attacking_goal)
                # desired_position = self.receive_point
                desired_position = self.robot.centre

            elif self.subtask == "intercept shot":
                if self.world_state.who_has_ball() is None:
                    enemy = self.robot.enemy1
                else:
                    enemy = self.world_state.who_has_ball()
                if enemy.facing_attacking_goal:
                    desired_position = Util.get_intercept_point_or_midpoint(
                        position, [enemy.position, Util.intercept_goal(enemy)])
                    # print("ENEMY CAN SCORE {}".format(desired_position))
                else:
                    desired_position = Util.get_intercept_point_or_midpoint(
                        position, [enemy.position, defending_goal_midpoint])
                    # print("ENEMY MIDPOINT {}".format(desired_position))

            elif self.subtask == "intercept pass":
                desired_position = Util.get_intercept_point_or_midpoint(position, self.robot.intercept_line)

            else:
                desired_position = None
            #
            # elif self.subtask == "align with ball" or self.subtask == "defend goal from ball":
            #     if self.world_state.ball_moving_to_defending_goal(self.robot.defending_goal):
            #         desired_position = self.world_state.ball_intercept_point(self.robot.defending_goal)
            #         if self.robot.defending_goal == "left":
            #             desired_position[0] += self.robot.GOAL_X_OFFSET
            #         else:
            #             desired_position[0] -= self.robot.GOAL_X_OFFSET
            #     else:
            #         ball_y = self.world_state.ball_position[1]
            #         if self.robot.defending_goal == "left":
            #             top = self.robot.defending_goal_line[0][1]
            #             bottom = self.robot.defending_goal_line[1][1]
            #             x = defending_goal_midpoint[0] + self.robot.GOAL_X_OFFSET
            #         else:
            #             top = self.robot.defending_goal_line[2][1]
            #             bottom = self.robot.defending_goal_line[3][1]
            #             x = defending_goal_midpoint[0] - self.robot.GOAL_X_OFFSET
            #         if ball_y <= top:
            #             y = top
            #         elif ball_y >= bottom:
            #             y = bottom
            #         else:
            #             y = ball_y
            #         desired_position = (x, y)
            #
            # else:  # elif self.subtask == "defend enemy shot":
            #     enemy = self.world_state.who_has_ball()
            #     intercept = Util.intercept_goal(enemy)
            #     if intercept is not None:
            #         if self.robot.defending_goal == "left":
            #             desired_position = (intercept[0] + self.robot.GOAL_X_OFFSET, intercept[1])
            #         else:
            #             desired_position = (intercept[0] - self.robot.GOAL_X_OFFSET, intercept[1])
            #     else:
            #         desired_position = defending_goal_midpoint
            #
            if desired_position is not None:
                self.robot.set_desired_position(desired_position)
                self.go_to_desired_position = True

        # ungrab and wait to receive the ball
        elif self.subtask == "receive pass":
            self.ungrab()

        if self.go_to_desired_position:
            distance_to_desired_position = self.robot.direction["desired position"]['distance']
            if not self.robot.within_tolerance_distance(distance_to_desired_position):
                # get to the desired position
                forward_angle_to_desired_position = Util.get_turn_angle(
                    heading, self.robot.direction["desired position"]['angle'])
                if abs(forward_angle_to_desired_position) <= 90:
                    if not self.robot.within_tolerance_angle(forward_angle_to_desired_position):
                        self.turn(forward_angle_to_desired_position, heading)
                    else:
                        self.move(distance_to_desired_position, position)
                else:
                    backward_angle_to_desired_position = Util.get_turn_angle(
                        Util.get_opposite_heading(heading), self.robot.direction["desired position"]['angle'])
                    if not self.robot.within_tolerance_angle(backward_angle_to_desired_position):
                        self.turn(backward_angle_to_desired_position, heading)
                    else:
                        self.move(-distance_to_desired_position, position)
            else:
                if self.subtask == "move to receive pass":
                    # turn to ally
                    angle_to_ally = Util.get_turn_angle(heading, self.robot.direction["ally"]['angle'])
                    if not self.robot.within_tolerance_angle(angle_to_ally):
                        self.turn(angle_to_ally, heading)
                    else:
                        self.ungrab()
                elif self.subtask == "intercept shot" or self.subtask == "intercept pass" or \
                        self.subtask == "defend enemy shot":
                    # turn to the enemy with the ball and ungrab
                    if self.world_state.who_has_ball() is None:
                        enemy_with_ball = 'enemy1'
                    else:
                        enemy_with_ball = self.world_state.who_has_ball().role
                    angle_to_enemy = Util.get_turn_angle(heading, self.robot.direction[enemy_with_ball]['angle'])
                    if not self.robot.within_tolerance_angle(angle_to_enemy):
                        self.turn(angle_to_enemy, heading)
                    else:
                        self.ungrab()
                else:  # self.subtask == "align with ball" or self.subtask == "defend goal from ball":
                    # turn to ball and ungrab
                    angle_to_ball = Util.get_turn_angle(heading, self.robot.direction["ball"]['angle'])
                    if not self.robot.within_tolerance_grab_angle(angle_to_ball):
                        self.turn(angle_to_ball, heading)
                    else:
                        self.ungrab()

    def subtask_complete(self, subtask):
        if subtask == "turn to ally":
            angle_to_ally = Util.get_turn_angle(self.robot.heading, self.robot.direction["ally"]['angle'])
            return self.robot.within_tolerance_angle(angle_to_ally)
        elif subtask == "move to receive pass":
            # if self.robot.desired_position == self.receive_point:
            angle_to_ally = Util.get_turn_angle(self.robot.heading, self.robot.direction["ally"]['angle'])
            distance_to_centre = Util.get_direction(self.robot.position, self.robot.centre)['distance']
            return self.robot.within_tolerance_angle(angle_to_ally) and \
                distance_to_centre <= 200
            # else:
            #     return False
        elif subtask == "has ball":
            return self.has_ball
        elif subtask == "kicked":
            return self.kicked

    def halt(self):
        if self.moving:
            self.com.halt()
            self.moving = False
            self.start_position = None
            self.start_heading = None

    def turn(self, angle, heading):
        if self.collision == "left" and angle < 0 or self.collision == "right" and angle > 0:
            return
        timestamp = time()
        if not self.moving and (heading != self.start_heading or timestamp - self.timestamps["turn"] > 3) and angle != 0:
            # print("HEADING: {}".format(heading))
            self.start_heading = heading
            self.timestamps["turn"] = timestamp
            self.moving = True
            if angle > 0:
                self.direction = "clockwise"
                self.com.turn_clockwise(self.angle_to_rotations(angle))
            elif angle < 0:
                self.direction = "anticlockwise"
                angle = abs(angle)
                self.com.turn_anticlockwise(self.angle_to_rotations(angle))

    def move(self, distance, position):
        if self.collision == "forward" and distance > 0:
            return
        timestamp = time()
        if not self.moving and (position != self.start_position or timestamp - self.timestamps["move"] > 3) and distance != 0:
            # print("POSITION: {}".format(position))
            self.start_position = position
            self.timestamps["move"] = timestamp
            self.moving = True
            if distance > 0:
                self.direction = "forward"
                self.com.move_forward(self.distance_to_rotations(distance))
            elif distance < 0:
                self.direction = "backward"
                distance = abs(distance)
                self.com.move_backward(self.distance_to_rotations(distance))

    def ungrab(self):
        if self.grabbed and self.timestamps["grab force"] >= 1:
            self.com.ungrab()
            self.timestamps["ungrabbed"] = time()
            self.grabbed = False

    def grab(self):
        if not self.grabbed:
            self.com.grab()
            self.grabbed = True

    def grab_force(self):
        print("[PLANNER GROUP 9] force grab (5 second timer ran out)")
        self.com.grab()
        self.grabbed = True
        self.timestamps["grab force"] = time()

    def kick_power(self, power):
        if not self.moving and not self.kicked:
            self.com.kick(power)
            self.timestamps["kicked"] = time()
            self.kicked = True
            self.grabbed = True
            self.has_ball = False

    def kick_distance(self, distance):
        # TODO - calibrate
        robot_length = 60
        power = 6.25 * ((distance / robot_length) + 1.8)
        if power < 22:
            power = 22
        elif power > 100:
            power = 100
        self.kick_power(power)

    @staticmethod
    def angle_to_rotations(angle):
        # TODO - calibrate
        angle *= 0.5
        rotations = int(round(angle * 260 / 360))
        if rotations < 20:
            rotations = 20
        elif 40 < rotations < 90:
            rotations -= 20
        elif 90 < rotations:
            rotations = 90
        print "[PLANNER GROUP 9] angle rotation ", rotations
        return rotations

    @staticmethod
    def distance_to_rotations(distance):
        # TODO - calibrate
        distance *= 0.3
        rotations = int(round(distance * 440 / 200))
        if rotations < 40:
            rotations = 15
        elif 40 < rotations < 400:
            rotations -= 20
        elif rotations <= 400:
            rotations = 400
        print "[PLANNER GROUP 9] distance rotation ", rotations
        return rotations


class RobotGroup09(BaseRobot):
    # TODO - calibrate
    TOLERANCE_ANGLE = 15
    TOLERANCE_DISTANCE = 15
    TOLERANCE_GRAB_ANGLE = 20
    GOAL_X_OFFSET = 0

    def __init__(self, name, role):
        BaseRobot.__init__(self, name, role)
        self.has_ball_sensor = False

        self.desired_position = None
        self.direction["desired position"] = None
        self.direction["enemy1"] = None
        self.direction["enemy2"] = None

        self.enemy1 = None
        self.enemy2 = None

        self.intercept_line = None

    def set_desired_position(self, desired_position):
        self.desired_position = desired_position
        try:
            if Util.point_in_defense_zone(self.zones, self.desired_position, self.attacking_goal) or \
                    (Util.point_in_defense_zone(self.zones, self.desired_position, self.defending_goal) and
                        Util.point_in_defense_zone(self.zones, self.ally.position, self.defending_goal)):
                # print("[PLANNER GROUP 9] desired position in a naughty place, retreating to centre")
                self.desired_position = self.centre
        except:
            pass

        self.direction["desired position"] = Util.get_direction(self.position,
                                                                self.desired_position)

    def set_enemies(self, enemy1, enemy2):
        self.enemy1 = enemy1
        self.enemy2 = enemy2

    def calculate_properties(self, *args):
        if self.visible:
            ball_position = args[0]
            BaseRobot.calculate_properties(self, ball_position)
            if self.desired_position is not None:
                self.direction["desired_position"] = Util.get_direction(self.position, self.desired_position)

            if self.enemy1.visible:
                self.direction["enemy1"] = Util.get_direction(self.position, self.enemy1.position)
            if self.enemy2.visible:
                self.direction["enemy2"] = Util.get_direction(self.position, self.enemy2.position)
            if self.enemy1.visible and self.enemy2.visible:
                self.intercept_line = [self.enemy1.position, self.enemy2.position]

    def within_tolerance_angle(self, angle):
        return abs(angle) < self.TOLERANCE_ANGLE

    def within_tolerance_grab_angle(self, angle_to_ball):
        return abs(angle_to_ball) < self.TOLERANCE_GRAB_ANGLE

    def within_tolerance_distance(self, distance):
        return distance < self.TOLERANCE_DISTANCE

    # def facing_robot(self, direction="forward"):
    #     """
    #     :param direction: our heading to use - should be forward or backward
    #     """
    #     if direction == "forward":
    #         heading = self.heading
    #     elif direction == "backward":
    #         heading = Util.get_opposite_heading(self.heading)
    #     else:  # direction == "clockwise" or direction == "anticlockwise":
    #         return None
    #
    #     other_visible_robots = []
    #     if self.enemy1.visible:
    #         other_visible_robots.append(self.enemy1)
    #     if self.enemy2.visible:
    #         other_visible_robots.append(self.enemy2)
    #     if self.ally.visible:
    #         other_visible_robots.append(self.ally)
    #     if other_visible_robots:
    #         list_of_obstacles = Util.get_blocking_robots(self, other_visible_robots, heading)
    #         if not list_of_obstacles:
    #             return None
    #         else:
    #             distance = 2000
    #             for robot in list_of_obstacles:
    #                 distance = min(distance, self.direction[robot.name]['distance'])
    #             assert distance != 2000
    #             return distance
    #     else:
    #         return None
