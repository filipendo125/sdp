from planner_group09 import RobotGroup09
from planner_group10 import RobotGroup10
from base_robot import EnemyRobot
from util_planning import Util


class WorldState:
    def __init__(self, vision, team_color, color_group09, color_group10):
        self.vision = vision
        self.defending_side = "left"
        self.goal_coords = None

        if team_color == "blue":
            enemy_team_color = "yellow"
        else:
            enemy_team_color = "blue"
        self.color_group09 = "{} + {}".format(team_color, color_group09)
        self.color_group10 = "{} + {}".format(team_color, color_group10)
        self.color_enemy1 = "{} + green".format(enemy_team_color)
        self.color_enemy2 = "{} + pink".format(enemy_team_color)
        self.robots = {'group09': RobotGroup09(self.color_group09, 'group09'),
                       'group10': RobotGroup10(self.color_group10, 'group10'),
                       'enemy1': EnemyRobot(self.color_enemy1, 'enemy1'),
                       'enemy2': EnemyRobot(self.color_enemy2, 'enemy2')}

        self.robots['group09'].set_ally(self.robots['group10'])
        self.robots['group10'].set_ally(self.robots['group09'])
        self.robots['enemy1'].set_ally(self.robots['enemy2'])
        self.robots['enemy2'].set_ally(self.robots['enemy1'])

        self.robots['group09'].set_enemies(self.robots['enemy1'], self.robots['enemy2'])
        self.robots['group10'].set_enemies(self.robots['enemy1'], self.robots['enemy2'])

        self.ball_position = (240, 240, 1)  # approximate centre

    def set_centre(self):
        centre = self.vision.get_centre()
        for _, robot in self.robots.iteritems():
            robot.set_centre(centre)

    def set_goals(self, defending_side=None):
        if defending_side is not None:
            self.defending_side = defending_side
        assert self.defending_side in ["left", "right"]

        self.goal_coords = self.vision.get_goal_positions()
        left_goal = "left"
        right_goal = "right"
        if self.defending_side == "left":

            self.robots['group09'].set_goals(left_goal, right_goal, self.goal_coords)
            self.robots['group10'].set_goals(left_goal, right_goal, self.goal_coords)
            self.robots['enemy1'].set_goals(right_goal, left_goal, self.goal_coords)
            self.robots['enemy2'].set_goals(right_goal, left_goal, self.goal_coords)
        else:
            self.robots['group09'].set_goals(right_goal, left_goal, self.goal_coords)
            self.robots['group10'].set_goals(right_goal, left_goal, self.goal_coords)
            self.robots['enemy1'].set_goals(left_goal, right_goal, self.goal_coords)
            self.robots['enemy2'].set_goals(left_goal, right_goal, self.goal_coords)

    # REVISITED
    def set_zones(self):
        # zone is a tuple of lists of point (in list)
        zones = self.vision.get_zones()
        zones = list(zones)
        for zone in range(0,len(zones)):
            zones[zone] = map(lambda element: tuple(element), zones[zone])
        for _, robot in self.robots.iteritems():
            robot.set_zones(zones)

    # REVISISTED
    def set_receive_zones(self):
        # this returns a list of lists
        zones = self.vision.get_zones()
        zones = list(zones)
        for zone in range(0,len(zones)):
            zones[zone] = map(lambda element: tuple(element), zones[zone])
        left_zone = zones[0]
        right_zone = zones[1]

        bottom_of_top_receive_zone = min(left_zone[i][1] for i in range(4))
        left_zone_right_side = max(left_zone[i][0] for i in range(4))
        right_zone_left_side = min(right_zone[i][0] for i in range(4))
        top_of_bottom_receive_zone = max(left_zone[i][1] for i in range(4))
        top_of_top_receive_zone = bottom_of_top_receive_zone/2
        bottom_of_bottom_receive_zone = top_of_bottom_receive_zone + bottom_of_top_receive_zone/2

        receive_zones = (top_of_top_receive_zone, bottom_of_top_receive_zone, top_of_bottom_receive_zone,
                         bottom_of_bottom_receive_zone, left_zone_right_side, right_zone_left_side)

        for _, robot in self.robots.iteritems():
            robot.set_receive_zones(receive_zones)

    def swap_goals(self):
        if self.defending_side == "left":
            self.set_goals("right")
        else:
            self.set_goals("left")

    def update(self):
        robot_list = self.vision.get_robots()
        for robot in robot_list:
            if robot.name == self.color_group09:
                self.robots['group09'].update(robot)
            elif robot.name == self.color_group10:
                self.robots['group10'].update(robot)
            elif robot.name == self.color_enemy1:
                self.robots['enemy1'].update(robot)
            elif robot.name == self.color_enemy2:
                self.robots['enemy2'].update(robot)

        current_ball_position = self.vision.get_ball_position()
        if current_ball_position is not None and current_ball_position[2]:  # if the ball is not detected, use the previous ball position
            self.ball_position = current_ball_position

        for _, robot in self.robots.iteritems():
            if robot.visible:
                robot.calculate_properties(self.ball_position)

    # REVISITED
    def has_ball_moved(self, old_ball_position):
        # check to see if the current ball position is sufficiently far enough away from the old_ball_position
        distance = Util.get_direction(self.ball_position, old_ball_position)['distance']
        if distance >= 5:
            return True
        else:
            return False

    # REVISITED
    def who_has_ball(self):
        if self.robots['group09'].has_ball_sensor:
            return self.robots['group09']
        if self.robots['group10'].has_ball_sensor:
            return self.robots['group10']
        return None

    # REVISITED
    def closest_robot_to_ball(self):
        closest = None
        min_distance = 2000
        for _, robot in self.robots.iteritems():
            if robot.visible:
                if robot.direction["ball"]['distance'] < min_distance:
                    min_distance = robot.direction["ball"]['distance']
                    closest = robot
        if closest is not None:
            return closest.role
        else:
            return None

    # REVISITED
    def ball_side(self):
        ball_x, ball_y, _ = self.ball_position
        width, height = self.vision.get_frame_size()

        if self.defending_side == "left":
            if ball_x < width/2:
                return "ours"
            else:
                return "theirs"
        else:
            if ball_x < width/2:
                return "theirs"
            else:
                return "ours"

    @staticmethod
    # REVISITED
    def heading_closer_to_attacking_goal_or_ally(robot):
        try:
            angle_to_score = Util.get_turn_angle(robot.heading, robot.direction["attacking goal"]['angle'])
            angle_to_ally = Util.get_turn_angle(robot.heading, robot.direction["ally"]['angle'])
        except:
            return "goal"

        if min(abs(angle_to_score), abs(angle_to_ally)) == abs(angle_to_score):
            return "goal"
        else:
            return "ally"
        # return "goal" or "ally"

    def ball_moving_to_defending_goal(self, goal):
        # project the direction the ball is currently heading onto a side of the pitch and check if this point
        ball_line = self.ball_line()

        # Return False if the ball is stationary (ball line length withing threshold)
        (x0, y0), (x1, y1) = ball_line
        if (x0 - x1)**2 + (y0 - y1)**2 < 5**2:
            return False

        width, height = self.vision.get_frame_size()
        width = self.goal_coords[2][0] - self.goal_coords[0][0]
        left_wall = ((self.goal_coords[0][0], 0), (self.goal_coords[0][0], height))
        right_wall = ((width, 0), (width, height))

        if goal == "left":
            return (Util.do_they_intersect(ball_line, left_wall) and ball_line[0][0] > ball_line[1][0])
        else:
            return (Util.do_they_intersect(ball_line, right_wall) and ball_line[0][0] < ball_line[1][0])
        # lies within our defending goal line

    def ball_goal_intercept_point(self, goal):
        ball_line = self.ball_line()

        width, height = self.vision.get_frame_size()
        width = self.goal_coords[2][0] - self.goal_coords[0][0]
        left_wall = ((self.goal_coords[0][0], 0), (self.goal_coords[0][0], height))
        right_wall = ((width, 0), (width, height))

        if goal == "left":
            return Util.line_intersection(ball_line, left_wall)
        else:
            return Util.line_intersection(ball_line, right_wall)

    def ball_line(self):
        """
        Returns the ball line
        :return:    ((origin_x, origin_y), (dest_x, dest_y))
        """

        ball = self.ball_position
        listof_previous_pos = self.vision.get_previous_ball_positions()
        # To convert to int: tuple(map(lambda element:int(element),listof_previous_pos[0]))

        prev_pos = listof_previous_pos[0]
        prev_prev_pos = listof_previous_pos[1]

        x, y = ball[0], ball[1]
        x_prev, y_prev = prev_pos
        x_prev_prev, y_prev_prev = prev_prev_pos

        ball_line = ((x_prev_prev, y_prev_prev),
                     (abs(x + 50 * (x_prev - x_prev_prev)), abs(y + 50 * (y_prev - y_prev_prev))))
        return ball_line

    def ball_line_y_val_at_x(self, x):
        """
        :return: The y value of ball line at point x
        """
        (x0, y0), (x1, y1) = self.ball_line()
        return (y1 - y0) / (x1 - x0) * (x - x0) + y0

    def handle_message(self, group, message):
        assert group in ['group09', 'group10']
        if group == 'group09':
            if message == "B":
                self.robots['group09'].has_ball_sensor = True
            elif message == "G":
                self.robots['group09'].has_ball_sensor = False
        else:
            if message == "B":
                self.robots['group10'].has_ball_sensor = True
            elif message == "G":
                self.robots['group10'].has_ball_sensor = False
