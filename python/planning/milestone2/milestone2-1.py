__author__ = 's1343880'

from pitch import *
from visionlauncher import *
from threading import Thread
from time import sleep
from communication import *

class Launcher:

    def __init__(self):
        self.vision = VisionLauncher(0)
        self.com = Communication()
        self.pitch = Pitch()

    def launch(self):
        self.vision.launch_vision()
        pass

    def begin_task1(self):
        t = Thread(target=self.task1)
        t.start()
        return t

    def task1(self):
        self.vision.wait_for_start()
        sleep (3)

        def update_ball_position():
            self.pitch.positions['ball'] = self.vision.get_ball_pos()

        def update_robot_position():
            self.pitch.positions['robot'] = self.vision.get_robot_midpoint(self.pitch.robot_color)
            
        def update_robot_direction():
            angle = self.vision.get_robot_direction(self.pitch.robot_color)
            angle = -1 * angle
            while -180 >= angle or angle > 180:
                if -180 >= angle:
                    angle += 360
                else:
                    angle -= 360
            self.pitch.directions['robot'] = angle

        update_ball_position()
        update_robot_position()
        update_robot_direction()

        # TODO DELETE
        # self.pitch.positions['ball'] = (23,23)
        # self.pitch.positions['robot'] = (46,46)
        # self.pitch.directions['robot'] = 0

        distance_angle = self.pitch.get_distance('robot', 'ball')
        turning_angle = self.pitch.get_turn_object('robot', distance_angle[1])

        self.com.ungrab()
        self.com.unkick()
        
        sleep(1)

        if turning_angle > 0:
            self.com.turn_clockwise(turning_angle)
        else:
            self.com.turn_anticlockwise(-1 * turning_angle)

        sleep(5)

        self.com.move_forward(int(distance_angle[0] * self.pitch.ratio))

        sleep(5)

        self.com.grab()







    def end(self):
        self.com.close()
        pass


def main():
    # task 1

    launcher = Launcher()
    launcher.begin_task1()
    launcher.launch()

    sleep(10)
    launcher.end()


    # p = Pitch()
    # action = 0;
    # speed = 45;
    # subtask_1_1 = False
    # subtask_1_2 = False
    # task = False
    #
    # # get position of the robot
    # goal = (0, 112)
    # # get position of the ball
    # region_a = (112, 196)
    #
    # # set the position of the ball and the robot
    # print 'task 1: Move and grab'
    # p.set_position('robot', goal)
    # p.set_position('ball', region_a)
    # p.set_direction('robot', 0)
    #
    # while not task:
    #     # update-position and direction of the ball
    #     # update-position and direction of the robot
    #     d = p.get_distance('robot', 'ball')
    #     t = p.get_turn_object('robot', d[1])
    #     p.simulate_turn('robot', t)
    #     print d
    #     if 3 < d[0] < 7:
    #         p.simulate_grab()
    #         task = True;
    #     elif d[0] > 7 + speed:
    #         p.simulate_move('robot', speed)
    #     elif d[0] > 7:
    #         p.simulate_ungrab()
    #         p.simulate_unkick()
    #         p.simulate_move('robot', d[0] - 6)
    #     else:
    #         p.simulate_move('robot', -10)
    #
    # print 'task 1: Move and grab Success'

main()
