__author__ = 's1343880'

import math
import sys
import os.path

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from communication import Communication

class Pitch:
    # topleft, topright,
    pitchBound = {}
    # position of the objects
    positions = {}
    # directions that the the objects are facing
    directions = {}

    robot = {}

    def __init__(self):
        # ratio; pixel * ratio = cm
        self.ratio = 1/2.6;
        self.pitchBound['TL'] = (0, 0)
        self.pitchBound['TR'] = (300, 0)
        self.pitchBound['BL'] = (0, 215)
        self.pitchBound['BR'] = (300, 215)
        self.positions['robot'] = (160, 160)
        self.positions['ball'] = (160, 170)
        self.directions['robot'] = 0
        # True if grabbing the ball
        self.robot['can_grab'] = True
        # False if it can kick the ball
        self.robot['can_kick'] = True
        self.positions['right_goal'] =  (568.0, 232.5)
        self.positions['left_goal']  = (5.5, 226.0)

        self.robot_color = 'yellow + green'

    # [0] gets the distance in 'pixel' between departure and destination
    # [1] gets 'absolute' angle where facing right in the screen is 0 degrees
    def get_distance(self, departure, destination):
        # angles:
        # right = 0 (origin)
        # down = -90
        # top = 90
        # left = 180
        # range: -180 < angle <= 180

        # hypotenuse = sqrt(opposite ^2 + adjacent^2);
        # y axis is reflected so opposite is different
        # x = adjacent
        # y = opposite
        x = self.positions[destination][0] - self.positions[departure][0]
        y = self.positions[departure][1] - self.positions[destination][1]
        # tan(angle) = opposite / adjacent
        # using atan2 look at the api for it in math
        distance = math.sqrt(x * x + y * y)
        angle = math.degrees(math.atan2(y, x))

        return distance, angle

    # getting how much the machine should turn
    # from direction its 'facing' at
    # to the 'angle' it wants to go
    def get_turn(self, facing, angle):
        turn_angle = facing - angle
        while -180 >= turn_angle or turn_angle > 180:
            if -180 >= turn_angle:
                turn_angle += 360
            else :
                turn_angle -= 360
        return turn_angle
        
    def get_turn_object(self, object_name, angle):
        return self.get_turn(self.directions[object_name], angle)

    # probably used only for testing
    def set_turn(self, object_name, turn_angle):
        angle = self.directions[object_name] - turn_angle
        while -180 >= angle or angle > 180:
            if -180 >= angle:
                angle += 360
            else:
                angle -= 360
        self.directions[object_name] = angle
        return angle

    def set_position(self, object_name, position):
        self.positions[object_name] = position
        return position

    def set_direction(self, object_name, direction):
        self.directions[object_name] = direction
        return direction

    def simulate_move(self, object_name, speed):
        distance = speed * self.ratio
        #print 'd',distance
        x = math.cos(math.radians(self.directions[object_name])) * distance
        #print 'x',x
        y = math.sin(math.radians(self.directions[object_name])) * distance
        #print 'y',y
        self.positions[object_name] = (self.positions[object_name][0] + x, self.positions[object_name][1] - y)
        print 'simulation: "',object_name,'" position', self.positions[object_name]

    def simulate_turn(self, object_name, turn):
        self.directions[object_name]
        self.set_turn(object_name, turn)
        print 'simulation: "',object_name,'" position', self.positions[object_name]

    def simulate_grab(self):
        self.robot['can_grab'] = False
        print 'simulation: The robot grabbed the ball'

    def simulate_ungrab(self):
        self.robot['can_grab'] = True
        print 'simulation: The robot ungrabbed'

    def simulate_kick(self):
        self.robot['can_kick'] = False
        print 'simulation: The robot kicked'

    def simulate_unkick(self):
        self.robot['can_kick'] = True
        print 'simulation: The robot unkicked'


    """
    def robot_move_forward(self, distance):
    def robot_turn(self, distance):
    """

# def main():
#     p = Pitch()
#
#     goal = (0, 240)
#     region_a = (280, 420)
#     region_b = (360, 60)
#
#     # task 1
#     print 'task 1: Move and grab'
#     p.set_position('robot', goal)
#     p.set_position('ball', region_a)
#     print 'robot', p.positions['robot']
#     print 'ball ', p.positions['ball']
#
#     distance = p.get_distance('robot', 'ball')
#     print distance
#     turn = p.get_turn(p.directions['robot'], distance[1])
#     print 'angle1: ', p.directions['robot']
#     print 'turn:   ', turn
#     print 'angle2: ', p.set_turn('robot', turn)
#
#     # task 2
#     print '\n'
#     print 'task 2: Turn, move and grab'
#     p.set_position('ball', region_b)
#     p.set_position('robot', region_a)
#     print 'robot', p.positions['robot']
#     print 'ball ', p.positions['ball']
#
#     distance = p.get_distance('robot', 'ball')
#     turn = p.get_turn(p.directions['robot'], distance[1])
#     print distance
#     print 'angle1: ', p.directions['robot']
#     print 'turn:   ', turn
#     print 'angle2: ', p.set_turn('robot', turn)
#
#     # task 3
#     print '\n'
#     print 'task 3: Turn and kick'
#     p.set_position('ball', goal)
#     p.set_position('robot', region_b)
#     print 'robot', p.positions['robot']
#     print 'ball ', p.positions['ball']
#
#     distance = p.get_distance('robot', 'ball')
#     turn = p.get_turn(p.directions['robot'], distance[1])
#     print distance
#     print 'angle1: ', p.directions['robot']
#     print 'turn:   ', turn
#     print 'angle2: ', p.set_turn('robot', turn)


#main()
