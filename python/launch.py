#!/usr/bin/env python
import sys
from launchers.launcher import *

try:
    with open("{}".format(sys.argv[1]), "r") as settings:
        main(settings.read().split("\n"))
except IndexError:
    main()
