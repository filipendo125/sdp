#!/usr/bin/env python

import sys
sys.path.append("../communications")
from communication_group09 import CommunicationGroup09
from terminal_display import TerminalDisplay


class RemoteControl:
    def __init__(self, usb_port_number):
        self.com = CommunicationGroup09(usb_port_number)
        self.com.open(self.handle_message)
        self.display = TerminalDisplay(self)

    def run(self):
        self.display.run()
        self.close()

    def close(self):
        self.display.close()
        self.com.close()

    @staticmethod
    def handle_message(message):
        print("MESSAGE RECEIVED: '{}'".format(message))

    def handle_input(self, character):
        self.com.send_command(character)


def main(usb_port_number):
    remote_control = RemoteControl(usb_port_number)
    try:
        remote_control.run()
    except KeyboardInterrupt:
        # if the user ctrl-C's or if anything else goes wrong, exit cleanly
        remote_control.close()
    except Exception as e:
        remote_control.close()
        raise e

if __name__ == '__main__':
    try:
        usb_port_number = sys.argv[1]
    except IndexError:
        usb_port_number = 0
    main(usb_port_number)
