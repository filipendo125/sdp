#!/usr/bin/env python

import os
import serial
import traceback

from threading import Thread
from communications.communication_group09 import CommunicationGroup09, SenderException
from communications.communication_group10 import CommunicationGroup10
from communications.terminal_display import TerminalDisplay

from planning.world_state import WorldState
from planning.planner_group09 import PlannerGroup09
from planning.planner_group10 import PlannerGroup10
from planning.strategy import Strategy
from planning.test_strategy_group09 import TestStrategyGroup09
from planning.test_strategy_group10 import TestStrategyGroup10

from vision.interface import VisionInterface


# Enable/disable Curses for IDE support
enable_curses = os.environ.has_key('NOT_DEBUG')

class Launcher:
    def __init__(self, color_settings, pitch_number, team_color, color_group09, color_group10, port_group09=None,
                 port_group10=None):
        assert port_group09 is not None or port_group10 is not None
        self.vision = VisionInterface(team_color, color_group09, color_group10,
                                      color_settings=color_settings, pitch=pitch_number, gui=True)

        world_state = WorldState(self.vision, team_color, color_group09, color_group10)
        if port_group09 is not None:
            self.com_group09 = CommunicationGroup09(port_group09)
            self.planner_group09 = PlannerGroup09(world_state, self.com_group09)
        if port_group10 is not None:
            self.com_group10 = CommunicationGroup10(port_group10)
            self.planner_group10 = PlannerGroup10(world_state, self.com_group10)

        try:
            self.strategy = Strategy(world_state, self.planner_group09, self.planner_group10)
        except AttributeError:
            try:
                self.strategy = TestStrategyGroup09(world_state, self.planner_group09)
            except AttributeError:
                self.strategy = TestStrategyGroup10(world_state, self.planner_group10)
        self.display = TerminalDisplay(self.strategy)

        self.main_thread = Thread(target=self.main_task, name="Main Thread")
        self.strategy_thread = Thread(target=self.strategy_task, name="Strategy Thread")

    def launch(self):
        try:
            self.main_thread.start()
            if enable_curses:
                self.display.run()
        except KeyboardInterrupt:
            if enable_curses:
                self.display.close()
        self.close()

    def main_task(self):
        if enable_curses:
            print("[LAUNCH] waiting for terminal display to start...")
        if not enable_curses or self.display.wait_for_start():
            self.strategy_thread.start()
            self.vision.launch_vision()
        else:
            self.close()

    def strategy_task(self):
        try:
            try:
                self.com_group09.open(self.strategy.handle_message_group09)
            except AttributeError:
                pass
        except (serial.SerialException, SenderException):
            traceback.print_exc()
        try:
            try:
                self.com_group10.open(self.strategy.handle_message_group10)
            except AttributeError:
                pass
        except (serial.SerialException, SenderException):
            traceback.print_exc()

        vision_launched = False
        while not self.display.closed and not vision_launched:
            print "[LAUNCH] waiting for vision to start..."
            vision_launched = self.vision.wait_for_start(1)
        if vision_launched:
            self.strategy.launch()

    def close(self):
        print("[LAUNCH] closing...")
        self.strategy.close()
        self.vision.close()
        try:
            self.com_group09.close()
        except AttributeError:
            pass
        try:
            self.com_group10.close()
        except AttributeError:
            pass
        self.display.close()


def expand(char):
    if char == "b":
        return "blue"
    elif char == "y":
        return "yellow"
    elif char == "g":
        return "green"
    elif char == "p":
        return "pink"


def main(settings=None):
    if settings is not None:
        # get settings from file
        color_settings = settings[0]
        assert color_settings in ["big", "small"]

        pitch_number = int(settings[1])
        assert pitch_number in [0, 1]

        team_color = settings[2]
        assert team_color in ["blue", "yellow"]

        color_group09 = settings[3]
        assert color_group09 in ["green", "pink"]

        color_group10 = settings[4]
        assert color_group10 in ["green", "pink"]

        port_group09 = settings[5]
        if port_group09 == "None":
            port_group09 = None
        else:
            port_group09 = int(port_group09)
        assert port_group09 is None or port_group09 in range(0, 4)

        port_group10 = settings[6]
        if port_group10 == "None":
            port_group10 = None
        else:
            port_group10 = int(port_group10)
        assert port_group10 is None or port_group10 in range(0, 4)

        launcher = Launcher(color_settings, pitch_number, team_color, color_group09, color_group10,
                            port_group09, port_group10)

    else:
        # get settings from user
        color_settings = raw_input("Color settings (b or s): ")
        assert color_settings in ["b", "s"]
        if color_settings == "b":
            color_settings = "big"
        else:
            color_settings = "small"

        pitch_number = int(raw_input("Pitch (0 for 3.D03, 1 for 3.D04): "))
        assert pitch_number in [0, 1]

        team_color = raw_input("Team colour (b for blue or y for yellow): ")
        assert team_color in ["b", "y"]
        team_color = expand(team_color)

        teams = raw_input("Group 9, 10 or both (type 9, 10 or b for both): ")
        assert teams in ["9", "10", "b"]

        if teams == "9":
            color_group09 = raw_input("Robot colour (singleton color, g for green or p for pink): ")
            assert color_group09 in ["g", "p"]
            color_group09 = expand(color_group09)
            if color_group09 == "green":
                color_group10 = "pink"
            else:
                color_group10 = "green"

            port_group09 = int(raw_input("Port: "))
            assert port_group09 in range(0, 4)

            launcher = Launcher(color_settings, pitch_number, team_color, color_group09, color_group10,
                                port_group09=port_group09)
        elif teams == "10":
            color_group10 = raw_input("Robot colour (singleton color, g for green or p for pink): ")
            assert color_group10 in ["g", "p"]
            color_group10 = expand(color_group10)
            if color_group10 == "green":
                color_group09 = "pink"
            else:
                color_group09 = "green"

            port_group10 = int(raw_input("Port: "))
            assert port_group10 in range(0, 4)

            launcher = Launcher(color_settings, pitch_number, team_color, color_group09, color_group10,
                                port_group10=port_group10)
        else:
            color_group09 = raw_input("Group 9's colour (singleton color, g for green or p for pink): ")
            assert color_group09 in ["g", "p"]
            color_group09 = expand(color_group09)
            if color_group09 == "green":
                color_group10 = "pink"
            else:
                color_group10 = "green"

            port_group09 = int(raw_input("Group 9's port: "))
            assert port_group09 in range(0, 4)
            port_group10 = int(raw_input("Group 10's port: "))
            assert port_group10 in range(0, 4)

            launcher = Launcher(color_settings, pitch_number, team_color, color_group09, color_group10,
                                port_group09, port_group10)

    launcher.launch()

